
<?php
session_start();
ob_start();
require_once("pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_COR_DIR . "utility.php");
require_once(PJI_COR_DIR . "admin-utility.php");
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Cosford Centres</title>
<!-- for-meta-tags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Health Plus Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-meta-tags-->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
</head>
	
<body>
<div class="main" id="home">
<!-- banner -->
		<div class="header_agileinfo">
						<div class="w3_agileits_header_text">
							 <!-- <ul class="top_agile_w3l_info_icons">
									<li><i class="fa fa-home" aria-hidden="true"></i>12K Street,New York City.</li>
									<li class="second"><i class="fa fa-phone" aria-hidden="true"></i>(+000) 123 456 87</li>
									
									<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:maria@example.com">info@example.com</a></li>
								</ul> -->

 
                        <img src="img/COSTFORD LOGO.png" width="65%" height="65%">

						</div>
						<div class="agileinfo_social_icons">
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"> </div>
			</div>				

		<div class="header-bottom">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<!-- <div class="logo">
						<h1><a class="navbar-brand" href="index.php"><span>H</span>ealth <i class="fa fa-plus" aria-hidden="true"></i> <p>Quality Care 4U</p></a></h1>
					</div> -->
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--sebastian">
					<ul id="m_nav_list" class="m_nav menu__list">
						<li class="m_nav_item menu__item" id="m_nav_item_1"> <a href="index.php" class="menu__link"> Home </a></li>
						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">About Us  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="founder.php">Founders & Leaders</a></li>
									<li><a href="history.php">History</a></li>
									<li><a href="approch.php">Approach</a></li>
									<li><a href="vissionmission.php">Vision, Mission</a></li>
									<li><a href="governing.php">Governing Body</a></li>
									<li><a href="typography.php">Sister Organisations</a></li>
								</ul>
						</li>			
						<li class="m_nav_item menu__item" id="moble_nav_item_4"> <a href="activities.php" class="menu__link">Activities  </a> </li>
						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Architecture  <b class="caret"></b></a> 
						  <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="projects.php">Projects</a></li>
									<li><a href="approach.php">Approach</a></li>
									<li><a href="material.php">Material & Technology</a></li>
									<li><a href="howbuild.php">How to build with Us</a></li>
								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Costford Centres  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="#">THRISSUR</a></li>
									<li><a href="thiruvananthapuram/thiruvananthapuram.php" target="_blank">THIRUVANANTHAPURAM</a></li>
									<li><a href="#">KOLLAM</a></li>
									<li><a href="kottayam/kottayam.php">KOTTAYAM</a></li>
									<li><a href="#">ALAPPUZHA</a></li>
									<li><a href="#">ERNAKULAM</a></li>
									<li><a href="#">THRIPRAYAR</a></li>
									<li><a href="#">THRISSUR 1</a></li>
									<li><a href="#">THRISSUR 2</a></li>
									<li><a href="#">SHORANUR</a></li>
									<li><a href="#">PALAKKAD</a></li>
									<li><a href="#">MALAPPURAM</a></li>
									<li><a href="#">KOZHIKODE</a></li>
								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_6 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Publications <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="publication.php">All Publications</a></li>
									<!-- <li><a href="otherpublication.php">Other Publications</a></li> -->
									<li><a href="newsevent.php">News & Events</a></li>
								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_6"> <a href="contact.php" class="menu__link"> Contact </a> </li>
					</ul>
				</nav>

				</div>
				<!-- /.navbar-collapse -->
			</nav>
	 </div>
</div>
<!-- banner -->
<!-- banner1 -->
	<div class="banner1 jarallax">
		<div class="container">
		</div>
	</div>

	<div class="services-breadcrumb">
		<div class="container">
			<ul>
				<li><a href="index.php">Home</a><i>|</i></li>
				<li>Costford centres</li>
			</ul>
		</div>
	</div>
<!-- //banner1 -->
	<div class="banner-bottom" id="about">
		<div class="container">
			<!-- <h2 class="w3_heade_tittle_agile">Contact Us</h2>
		    <p class="sub_t_agileits">Get in touch...</p> -->

           <div class="contact-top-agileits">
           	<p style="color: #b90e0e;font-size: 2em;letter-spacing: 0px;">Main Office</p> 
<?php 
 	$where 		= "b_active='1' and status = 1";
    $rows 		= select_all_rows("costford_inner_centre", $where, $conn, true);
    $slno 		= 1; 
    if(isset($rows))
  	{
    	foreach($rows as $row)
    	{
	      	$id   		= $row['centre_id'];
	      	$type 		= $row['centre_type'];
			$place 		= $row['centre_place'];
			$address 	= $row['centre_address'];
			$phone 		= $row['centre_phone'];
			$fax 		= $row['centre_fax'];
			$email 		= $row['centre_email'];	
			if($type == 'main')
			{				     	
	      ?>
	      		    

	      	<div class="col-md-4 contact-grid " style="margin-top: 1%;height: 253px; ">
					<div class="contact-grid1 agileits-w3layouts">
						<i class="fa fa-location-arrow"></i>
						<div class="con-w3l-info">
						   <h4 style="font-size: 1.3em;"><?php echo $place?> </h4>
						  
						  <p><?php echo $address?></span></p>
						  <p>Phone No :<span><?php echo $phone?></span></p>
						  <p>Fax :<span><?php echo $fax?></span></p>
						  <p><span><?php echo $email?></span></p>
						</div>
						<div class="clearfix"></div>
					</div>
			</div>

	      <?php
	  }
	    }
	}
?>
               
				

				<div class="clearfix"></div>
				
			</div>

			 <div class="contact-top-agileits">
			 	 <p class="" style="color: #b90e0e;font-size: 2em;letter-spacing: 0px;">Branches</p>
<?php 
 	$where 		= "b_active='1' and status = 1";
    $rows 		= select_all_rows("costford_inner_centre", $where, $conn, true);
    $slno 		= 1; 
    if(isset($rows))
  	{
    	foreach($rows as $row)
    	{
	      	$id   		= $row['centre_id'];
	      	$type 		= $row['centre_type'];
			$place 		= $row['centre_place'];
			$address 	= $row['centre_address'];
			$phone 		= $row['centre_phone'];
			$fax 		= $row['centre_fax'];
			$email 		= $row['centre_email'];	
			if($type == 'branch')
			{				     	
	      ?>
	       
	      	<div class="col-md-4 contact-grid " style="margin-top: 1%;height: 253px;">
					<div class="contact-grid1 agileits-w3layouts">
						<i class="fa fa-location-arrow"></i>
						<div class="con-w3l-info">
						   <h4 style="font-size: 1.3em;"><?php echo $place?> </h4>
						  
						  <p><?php echo $address?></span></p>
						  <p>Phone No :<span><?php echo $phone?></span></p>
						  <p>Fax :<span><?php echo $fax?></span></p>
						   <p><span><?php echo $email?></span></p>
						</div>
						<div class="clearfix"></div>
					</div>
			</div>

	      <?php
	  }
	    }
	}
?>
               
				

				<div class="clearfix"></div>
				
			</div>

				
				<div class="clearfix"></div>
				
			</div>
			
	    </div>
	</div>

<!-- <div class="map_agile">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.84010033106!2d-74.25819252532891!3d40.70583163828471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1475140387172" style="border:0"></iframe>

 -->
</div>
<!-- footer -->
	<div class="footer">
		<div class="container">
			
			<div class="agile_footer_copy">
				<div class="w3agile_footer_grids">
					<div class="col-md-4 w3agile_footer_grid">
						<h3>About Us</h3>
						<p style="text-align: justify;">Centre of Science and Technology For Rural Development (COSTFORD) is a Thrissur city-based organisation that gives technological assistance to people in alternative building technology.</p>
					</div>
					<div class="col-md-4 w3agile_footer_grid">
						<h3>Contact Info</h3>
						<ul>
							<li><i class="fa fa-map-marker" aria-hidden="true"></i>The Hamlet, Benedict Nagar,
Nalanchira P.O, <span>Thiruvananthapuram 695015</span></li>
							<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@example.com">costfordtvm@gmail.com</a></li>
							<li><i class="fa fa-phone" aria-hidden="true"></i>0471 - 2530031, 09446540220</li>
						</ul>
					</div>
					<div class="col-md-4 w3agile_footer_grid w3agile_footer_grid1">
						<h3>Navigation</h3>
						<ul>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="projects.php">Architecture</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="centres.php">Costford Centres</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="history.php">About</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="contact.php">Contact Us</a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<!--<div class="w3_agileits_copy_right_social">
				<div class="col-md-6 agileits_w3layouts_copy_right">
					<p>&copy; 2020. All rights reserved | Design by <a href="http://grameena.org/">Grameena Patana Kendram</a></p>
				</div>
				<div class="col-md-6 w3_agile_copy_right">
					<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
				</div>
				<div class="clearfix"> </div>
			</div>-->
		</div>
	</div>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
 <!-- js -->
<script src="js/jquery-2.2.3.min.js"></script>
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Health Plus
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="images/g9.jpg" alt=" " class="img-responsive" />
						<p>Ut enim ad minima veniam, quis nostrum 
							exercitationem ullam corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi consequatur? Quis autem 
							vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur, vel illum qui 
							dolorem eum fugiat quo voluptas nulla pariatur.
							<i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
								esse quam nihil molestiae consequatur.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
			<script src="js/jarallax.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>
	
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>