<?php


/*******************************/	
// 		function starts		   //
/******************************/ 

//Check admin login session function
function check_login()
	 {
		if((empty($_SESSION['prjt_nam']['admin_id']))&&(empty($_SESSION['prjt_nam']['admin_username'])))
			{
				header('Location: index.php?desg='.get_current_url());
			}
			else
			{
				return true;
			}
	}
	function user_login()
	{
		if(empty($_SESSION['user_id']))
			{
				header('Location: index.php?desg='.get_current_url());
			}
			else
			{
				return true;
			}
	}
//Get not login home page 
function check_admin_is_login($desg_url="home.php")
	{
		if((!empty($_SESSION['prjt_nam']['admin_id']))&&(!empty($_SESSION['prjt_nam']['admin_username'])))
			{				
				header('Location: '.$desg_url);
			}	
	}

function admin_login_main($conn,$table,$username,$password,$desg_url="home.php")
	{
		
		$table_main=$table['login'];
		$table_history=$table['history'];
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($conn,$username);
		$password = mysqli_real_escape_string($conn,$password);
		echo $login_cause=" admin_username='".$username."' AND  admin_password='".$password."'";
		$sql_log_user=select_a_row($table_main,$login_cause,$conn,true);   //check the username and password
		$count=count($sql_log_user);
		// If result matched $username and $password, table row must be 1 row
		if($count==1) //Success login
		{ 
			  if(!empty($table_history)||($table_history!=false))
			  {
					$data['admin_history_ip'] = get_ip();
					$data['admin_history_result'] = "Sucess";
					$data['admin_history_user_id'] = $sql_log_user[0]['admin_id'];
					$data['admin_history_status'] = "1";	 //admin_history_status	 
					$sql_log_history=insert_data($table_history,$data,true,$conn); 
			  }
			$_SESSION['prjt_nam']['admin_id']		=$sql_log_user[0]['admin_id'];
			$_SESSION['prjt_nam']['admin_username']	=$sql_log_user[0]['admin_username'];	
			//$_SESSION['prjt_nam']['admin_password']	=$sql_log_user[0]['admin_password'];
			$desg_url=urldecode($desg_url);				
			header('Location: '.$desg_url);
			return true;
		}
		else //Error login
		{
			   if(!empty($table_history)||($table_history!=false))
			  {
					$data['admin_history_ip'] = get_ip();
					$data['admin_history_result'] = "FAIL";
					$data['admin_history_user_id'] = "0";
					$data['admin_history_status'] = "1";	 //admin_history_status	 
					$sql_log_history=insert_data($table_history,$data,true,$conn); 
			  }
			  if($desg_url=="home.php")
			  {
				 header("location:?msg=Invalid Username/Password");  
			  }
			  else
			  {
				header("location:?msg=Invalid Username/Password.&desg=".$desg_url); 
			  }
			 
			  return false;
		}
	} //admin_login_main ENDS
function get_admin($typ='id')
{
	if($typ=='id')
	{
		return $_SESSION['prjt_nam']['admin_id'];
	}
	else if($typ=='user')
	{
		return $_SESSION['prjt_nam']['admin_username'];		 
	}
	else if($typ=='pass')
	{
		return $_SESSION['prjt_nam']['admin_password'];			 
	}
	else
	{
		//return false;		
		return $_SESSION['prjt_nam']['admin_id']; 
	}
}
function get_albm_name($albmid,$table,$sfx,$conn)
{
	$row_alb=select_a_row($table,"b_active >0 AND pk_id=$albmid ORDER BY pk_id DESC",$conn,true);
	$albname=$row_alb[0][$sfx.'name'];
	return($albname);

}
//Its Return video id
function create_video_link($link,$type)
	{
		switch($type)
		{
			case('youtube'):$string = $link;
							$output = preg_split("/youtu.be\//",$string, 2); //http://youtu.be/N4rhgX9olmE
							return($output[1]);
							break;
			case('vimeo'):	$string = $link;
							$output = preg_split("/vimeo.com\//",$string, 2); //http://youtu.be/N4rhgX9olmE
							return($output[1]);
							break;
		 case('metacafe'):	$string = $link;
							$output = preg_split("/watch\//",$string, 2); //http://www.metacafe.com/embed/10620165/
							return($output[1]);
							break;				
							default:$string = $link;
							$output = preg_split("/youtu.be\//",$string, 2); //http://youtu.be/N4rhgX9olmE
							return($output[1]);
							break;
		}
		return $output[1];
		
	}
function create_video($link,$provider,$w,$h)
	{
		switch ($provider) {
			case 'youtube':
				$video='<iframe width="'.$w.'" height="'.$h.'" src="//www.youtube.com/embed/'.$link.'" frameborder="0" allowfullscreen></iframe>';
				break;
			case 'vimeo':
				$video='<iframe src="//player.vimeo.com/video/'.$link.'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffeb0f" width="'.$w.'" height="'.$h.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
				break;
			case 'metacafe':	
			$video='<iframe src="http://www.metacafe.com/embed/'.$link.'" width="'.$w.'" height="'.$h.'" allowFullScreen frameborder=0></iframe>';
			break;			
			default:
			   $video='<iframe width="'.$w.'" height="'.$h.'" src="//www.youtube.com/embed/'.$link.'" frameborder="0" allowfullscreen></iframe>';
			}
			return($video);
	}
function show_video_link($type,$link)
	{
		switch($type)
		{
			case('youtube'):$mainlink="http://youtu.be/";
			break;
			case('vimeo'):$mainlink="http://vimeo.com/";
			break;
			case('metacafe'):$mainlink="http://metacafe.com/watch/";
			break;
			default:$mainlink="http://youtu.be/";
			break;
			
		}
		return($mainlink);
	}
/************************/	
//	functions Ends 	 	//
/************************/	

?>