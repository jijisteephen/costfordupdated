<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once("../".PJI_COR_DIR . "utility.php");
require_once("../".PJI_COR_DIR . "admin-utility.php");
$dyn_folder = "../".PJI_IMG_DIR . PJI_ARA_DIR ;
$dyn_folder1 = "../".PJI_IMG_DIR . PJI_APR_DIR ;
?>
<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Costford - Projects</title>
<!-- for-meta-tags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Health Plus Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-meta-tags-->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- Portfolio-CSS -->	<link rel="stylesheet" href="../css/swipebox.css" type="text/css" media="all">
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script> -->
<script type="text/javascript">
	$(document).ready(function()
	{
		$("#location").change(function()
		{
			var location = $("#location").val();
			window.location.href = "location_search.php?location="+location;

		});
		$("#sqfeet").change(function()
		{
			var sqfeet = $("#sqfeet").val();
			window.location.href = "sqfeet_search.php?sqfeet="+sqfeet;
		
		});
		$("#rate").change(function()
		{
			var rate = $("#rate").val();
			window.location.href = "rate_search.php?rate="+rate;
		});
	})
</script>
<style>
#MyForm{
 width: 100%;
    border: 1px solid #e68787;
    padding: 14px;
    background: #fde5e5;
}	
#Myimg{
	margin:0 auto;
  	background: #ccc;
    border: 1px solid #000;
    padding: 9px;
}
</style>
</head>
<script>
$(document).ready(function(){
	$(".closeclose").click(function()
	{
		location.reload();

	});
	
	$(".gallery-top").click(function()
		{
			var ID = $(this).attr("id");
			$.getJSON( "gallerymodal_json.php?projectid="+ID, function( json ) 
			{
				var len=json.length;
				for(var i=0;i<len;i++)
				{
					var project_name=json[i].project_name;
					var project_type=json[i].project_type;
					var project_cname=json[i].project_cname;
					var project_ctype=json[i].project_ctype;
					var project_canname=json[i].project_canname;
					var project_locality=json[i].project_locality;
					var project_district=json[i].project_district;
					var project_state=json[i].project_state;
					var project_subcentre=json[i].project_subcentre;
					var project_service=json[i].project_service;
					var project_year=json[i].project_year;
					var project_cost=json[i].project_cost;
					var project_currentcost=json[i].project_currentcost;
					var project_area=json[i].project_area;
					var project_specific=json[i].project_specific;
					var project_found=json[i].project_found;
					var project_super=json[i].project_super;
					var project_roof=json[i].project_roof;
					var project_doors=json[i].project_doors;
					var project_floor=json[i].project_floor;
					var project_any=json[i].project_any;
					var project_brief=json[i].project_brief;
					var image_image = json[i].image_image;
					var image_image1 = json[i].image_image1;
					var image_image2 = json[i].image_image2;
					var image_image3 = json[i].image_image3;
					var image_image4 = json[i].image_image4;
					var image_image5 = json[i].image_image5;
					$("#image11").click(function()
					{
						
						$("#href_image").attr("href", '../images/project/'+image_image+'' );
					});
					$("#image22").click(function()
					{
						$("#href_image1").attr("href", '../images/project/'+image_image1+'' );
					});
					$("#image33").click(function()
					{
						$("#href_image2").attr("href", '../images/project/'+image_image2+'' );
					});
					$("#image44").click(function()
					{
						$("#href_image3").attr("href", '../images/project/'+image_image3+'' );
					});
					$("#image55").click(function()
					{
						$("#href_image4").attr("href", '../images/project/'+image_image4+'' );
					});
					$("#image66").click(function()
					{
						$("#href_image5").attr("href", '../images/project/'+image_image5+'' );
					});

					

				
					$('#project_name').html(project_name);
					$('#project_type').html(project_type);
					$('#project_cname').html(project_cname);
					$('#project_ctype').html(project_ctype);
					$('#project_canname').html(project_canname);
					$('#project_locality').html(project_locality);
					$('#project_district').html(project_district);
					$('#project_state').html(project_state);
					$('#project_subcentre').html(project_subcentre);
					$('#project_service').html(project_service);
					$('#project_year').html(project_year);
					$('#project_cost').html(project_cost);
					$('#project_currentcost').html(project_currentcost);
					$('#project_area').html(project_area);
					$('#project_specific').html(project_specific);
					$('#project_found').html(project_found);
					$('#project_super').html(project_super);
					$('#project_roof').html(project_roof);
					$('#project_doors').html(project_doors);
					$('#project_floor').html(project_floor);
					$('#project_any').html(project_any);
					$('#project_brief').html(project_brief);
					$('#image_image').append("<img  src='../images/project/"+image_image+"'>");
					$('#image_image1').append("<img src='../images/project/"+image_image1+"'>");
					$('#image_image2').append("<img src='../images/project/"+image_image2+"'>");
					$('#image_image3').append("<img src='../images/project/"+image_image3+"'>");
					$('#image_image4').append("<img src='../images/project/"+image_image4+"'>");
					$('#image_image5').append("<img src='../images/project/"+image_image5+"'>");
					
					
				}
			});
		});

		

	$('.imgmodal').click(function(){
  	$('#Mymodal').modal('show')
	});

});
</script>
<body>
<div class="main" id="home">
<!-- banner -->
		<div class="header_agileinfo">
						<div class="w3_agileits_header_text">
								<!-- <ul class="top_agile_w3l_info_icons">
									<li><i class="fa fa-home" aria-hidden="true"></i>12K Street,New York City.</li>
									<li class="second"><i class="fa fa-phone" aria-hidden="true"></i>(+000) 123 456 87</li>
									
									<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:maria@example.com">info@example.com</a></li>
								</ul> -->
								<img src="../img/COSTFORD LOGO.png" width="65%" height="65%">

						</div>
						<div class="agileinfo_social_icons">
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"> </div>
			</div>				

			<div class="header-bottom">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<!-- <div class="logo">
						<h1><a class="navbar-brand" href="index.php"><span>H</span>ealth <i class="fa fa-plus" aria-hidden="true"></i> <p>Quality Care 4U</p></a></h1>
					</div> -->
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--sebastian">
					<ul id="m_nav_list" class="m_nav menu__list">
						<li class="m_nav_item menu__item menu__item" id="m_nav_item_1"> <a href="../index.php" class="menu__link"> Home </a></li>
						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">About Us  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../founder.php">Founders & Leaders</a></li>
									<li><a href="../history.php">History</a></li>
									<li><a href="../approch.php">Approach</a></li>
									<li><a href="../vissionmission.php">Vision,Mission</a></li>
									<li><a href="../governing.php">Governing Body</a></li>
									<li><a href="../typography.php">Sister Organisations</a></li>
								</ul>
						</li>		
						<li class="m_nav_item menu__item" id="moble_nav_item_4"> <a href="../activities.php" class="menu__link">Activities  </a> </li>

						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Architecture  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../projects.php">Projects</a></li>
									<li><a href="../approach.php">Approach</a></li>
									<li><a href="../material.php">Material & Technology</a></li>
									<li><a href="../howbuild.php">How to build with Us</a></li>
								</ul>
						</li>
						
						<li class="m_nav_item menu__item--current" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Costford Centres  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="#">THRISSUR</a></li>
									<li><a href="thiruvananthapuram.php" target="_blank">THIRUVANANTHAPURAM</a></li>
									<li><a href="#">KOLLAM</a></li>
									<li><a href="../kottayam/kottayam.php">KOTTAYAM</a></li>
									<li><a href="#">ALAPPUZHA</a></li>
									<li><a href="#">ERNAKULAM</a></li>
									<li><a href="#">THRIPRAYAR</a></li>
									<li><a href="#">THRISSUR 1</a></li>
									<li><a href="#">THRISSUR 2</a></li>
									<li><a href="#">SHORANUR</a></li>
									<li><a href="#">PALAKKAD</a></li>
									<li><a href="#">MALAPPURAM</a></li>
									<li><a href="#">KOZHIKODE</a></li>

								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_6 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Publications <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../publication.php">All Publications</a></li>
									<!-- <li><a href="otherpublication.php">Other Publications</a></li> -->
									<li><a href="../newsevent.php">News & Events</a></li>
								</ul>
						</li>
						
						<li class="m_nav_item menu__item" id="moble_nav_item_6"> <a href="../contact.php" class="menu__link"> Contact </a> </li>
					</ul>
				 </nav>
				</div>
				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1" >
					<nav class="menu menu--sebastian">
					<ul id="m_nav_list" class="m_nav menu__list" style="font-family:'Open Sans', Arial, sans-serif;margin-right:27px; font-size: 86%;margin-top: .8%;color: #520203;">
						<a href ="thiruvananthapuram.php" style="color: #520203;"><b>THIRUVANANTHAPURAM CENTRE</b></a>
					</ul>
				</nav>

				</div>
				<!-- /.navbar-collapse -->
			</nav>
	 </div>
</div>
<!-- banner -->
<!-- banner1 -->

<!-- //banner1 -->
<style type="text/css">
	
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
	font-size: 1em;
    color: #0a0909;
    border-top: none !important;
    padding: 0 !important;
    border: 1px solid #d8bbba;
}
.selectclass {
    height: 23px;
    width: 10%;
    text-align: center;
    background-color: #520203;
    color: #fff;
    margin-bottom: 1%;
    float: left;
    border-radius: 5px;
    font-size: 11px;
}

.table-bordered {
    border: 1px solid #d8bbba;
}	
.content-wrap section {
    display: none;
    margin: 0 auto;
    padding: 0;
     text-align: left; 
}
</style>

<!-- Portfolio -->
	<div class="portfolio" id="specials" style="padding: 0em 0px;">
			<style type="text/css">
		.btttn
		{
			margin-left: 7.9%;
    border-radius: 7px;
  
    color: #fff;
    height: 23px;
    background-color: #520203;
    width: 4%;
    font-size: 11px;
		}
	</style>
<a href="thiruvananthapuram-projects.php">
	<input type="button" name="" value="<< Back" class="btttn">

</a>
			<div class="container">
				<!-- <h2 class="w3_heade_tittle_agile"></h2> -->
		    <!-- <p class="sub_t_agileits">Quality Care...</p> -->
<nav>
					<select style="margin-left: 67%" class="selectclass" id="location">
						<option value="select">Sort by location</option>
						<option value="Thiruvananthapuram">Thiruvananthapuram</option>
					</select>
					<select style="margin-left: 1%" class="selectclass" id="rate">
						<option>Sort by currentrates</option>
						<option value="1">10 lakhs to 20 lakhs</option>
						<option value="2">20 lakhs to 40 lakhs</option>
						<option value="3">40 lakhs to 60 lakhs</option>
						<option value="4">60 lakhs to 80 lakhs</option>
						<option value="5">80 lakhs to 1 crore</option>
						<option value="6">Above 1 crore</option>
					</select>
					<select style="margin-left: 1%"  class="selectclass" id="sqfeet">
						<option value="select">Sort by sqft</option>
						<option value="1">500 to 1500 </option>
						<option value="2">1500 to 2500 </option>
						<option value="3">2500 to 3000 </option>
						<option value="4">3000 to 5000 </option>
						<option value="5"> Above 5000 </option>
					</select>
				</nav>
				<?php 
$rate = $_REQUEST['rate'];
if($rate == 1)
{
	$rateview = '10 lakhs to 20 lakhs';
}
else if($rate == 2)
{
	$rateview = '20 lakhs to 40 lakhs';
}
else if($rate == 3)
{
	$rateview = '40 lakhs to 60 lakhs';
}
else if($rate == 4)
{
	$rateview = '60 lakhs to 80 lakhs';
}
else if($rate == 5)
{
	$rateview = '80 lakhs to 1 crore';
}
else if($rate == 6)
{
	$rateview = 'Above 1 crore';
}




?>
					
			<div class="tabs tabs-style-bar">
				<nav>
					<ul>
						<li><a href="#section-bar-1" class="icon icon-box"><span>Current cost : <?php echo $rateview ?></span></a></li>
						<!-- <li><a href="#section-bar-2" class="icon icon-display"><span>Private</span></a></li> -->
						<!-- <li><a href="#section-bar-3" class="icon icon-upload"><span>Institutional</span></a></li>
						<li><a href="#section-bar-4" class="icon icon-upload"><span>Social Housing</span></a></li> -->
					</ul>
				</nav>

				<div class="content-wrap">

				<section id="section-bar-1" class="agileits w3layouts">
						<div class="gallery-grids">
						<?php 
						if($rate == 1)
						{
							$btwen = 'project_currentcost BETWEEN 1000000 And 2000000';
						}
						else if($rate == 2)
						{
							$btwen = 'project_currentcost BETWEEN 2000000 And 4000000';
						}
						else if($rate == 3)
						{
							$btwen = 'project_currentcost BETWEEN 4000000 And 6000000';
						}
						else if($rate == 4)
						{
							$btwen = 'project_currentcost BETWEEN 6000000 And 8000000';
						}
						else if($rate == 5)
						{
							$btwen = 'project_currentcost BETWEEN 8000000 And 10000000';
						}
						else if($rate == 6)
						{
							$btwen = 'project_currentcost > 10000000';
						}
 	                                     $where="b_active='1' and status = 1 and project_type='Residential' and project_place='Thiruvananthapuram' and ".$btwen." ";
                                          $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     $image  = $row['project_frontimage']; 
	                                     
	                                     ?>
							<div class="col-md-4 col-sm-4 gallery-top"  id="<?php echo($row['project_id'])?>" style="margin-top:5px;padding: 0px;">
								
										<img src="<?php echo($dyn_folder.$image);?>" alt="Costford" class="img-responsive imgmodal" id="imgmodal" style="cursor: pointer;height: 226px;width: 99%">

										<figcaption></figcaption>
									<!-- </figure> -->
								<!-- </a> -->
							</div><?php }}?>
							
							
							<!-- .modal -->
							<div class="modal fade" id="Mymodal" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close closeclose" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
 					
					<table class="table table-bordered">
					
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td id="project_name"></td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td id="project_type"></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td id="project_cname"></td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td id="project_ctype"> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td id="project_canname"></td>
							</tr>
							<tr>
								<td>Locality</td>
								<td id="project_locality"></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td id="project_district"></td>
							</tr>
							<tr>
								<td>State</td>
								<td id="project_state">  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td id="project_subcentre"></td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td id="project_service"></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td id="project_year"></td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td id="project_cost"></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td id="project_currentcost"></td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td id="project_area"></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td id="project_specific">   </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td id="project_found"></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td id="project_super"></td>
							</tr>
							<tr>
								<td>Roof</td>
								<td id="project_roof"></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td id="project_doors"></td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td id="project_floor"> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr >
								<td>Any other</td>
								<td id="project_any">      </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td id="project_brief">       </td>
							</tr>
							
						</tbody>
					
					</table>                        
				</div>
				
			   <div class="clearfix"> </div>
			</div>	
		
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="https://www.w3schools.com" class="swipebox" id="href_image">
									<figure class="effect-apollo">
										<div id="image_image" style="height: 226px;;width: 98%"></div>
										<figcaption id="image11"></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top" >
								<a href="<?php echo($dyn_folder1.$image1);?>" class="swipebox"  id="href_image1">
								<figure class="effect-apollo">
										<div id="image_image1" style="height: 226px;;width: 98%"></div>
										<figcaption id="image22"></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image2);?>" class="swipebox" id="href_image2">
								<figure class="effect-apollo">
										<div id="image_image2" style="height: 226px;;width: 98%"></div>
										<figcaption id="image33"></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image3);?>" class="swipebox" id="href_image3">
								<figure class="effect-apollo">
										<div id="image_image3" style="height: 226px;;width: 98%"></div>
										<figcaption id="image44"></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image4);?>" class="swipebox" id="href_image4">
								<figure class="effect-apollo">
										<div id="image_image4" style="height: 226px;;width: 98%"></div>
										<figcaption id="image55"></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image5);?>" class="swipebox" id="href_image5">
								<figure class="effect-apollo">
										<div id="image_image5" style="height: 226px;;width: 98%"></div>
										<figcaption id="image66"></figcaption>
									</figure>
								</a>
										</div>
							
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default closeclose" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
							<!-- Modal 1 ends  -->
							
							
							<div class="clearfix"></div>
						</div>
					</section>


					<!-- //Tab-1 -->

					<!-- Tab-2 -->
					<section id="section-bar-2" class="agileits w3layouts">
						<div class="gallery-grids">
							<div class="col-md-4 col-sm-4 gallery-top" style="margin-bottom: 10px;">
								<!-- <a href="../images/approach/DSC_0920.jpg" class="swipebox">
									<figure class="effect-apollo"> -->
										<img src="../images/approach/DSC_0920.jpg" alt="Costford" class="img-responsive" id="primodal" style="cursor: pointer;height:226px;width:99%">
										<figcaption></figcaption>
									<!-- </figure>
								</a> -->
							</div>

							<div class="col-md-4 col-sm-4 gallery-top" style="margin-bottom: 10px;">
								<!-- <a href="../images/approach/5.jpg" class="swipebox">
									<figure class="effect-apollo"> -->
										<img src="../images/approach/5.jpg" alt="Costford" class="img-responsive" id="primodal1" style="cursor: pointer;height:226px;width:99%">
										<figcaption></figcaption>
									<!-- </figure>
								</a> -->
							</div>
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g2.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g2.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g3.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g3.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g5.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g5.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g6.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g6.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
 <!-- .modal1 Starts -->
<div class="modal fade" id="private" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
 					
					<table class="table table-bordered">
						
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td>Institute of Management in Government hostel</td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td>Institutional</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td>Government </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td>Institute of Management in Government </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td>Yes </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td>Thiruvananthapuram </td>
							</tr>
							<tr>
								<td>State</td>
								<td>Kerala  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td>Consultancy and construction / Construction only / DPR only / PMC</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td></td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td> </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td>    </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td></td>
							</tr>
							<tr>
								<td>Roof</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td></td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr>
								<td>Any other</td>
								<td>        </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>     </td>
							</tr>
							
						</tbody>
						
					</table>                   
				</div>
				
			   <div class="clearfix"> </div>
			</div>	
			
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0838.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0838.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top" >
								<a href="../images/img/DSC_0920.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0920.jpg" alt="Costford" class="img-responsive" style="height: 236px;">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0919.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0919.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0921.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0921.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0870.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0870.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0846.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0846.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div>
							
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>

<!-- modal 2 starts -->
<div class="modal fade" id="private1" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
 					
					<table class="table table-bordered">
						
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td>Karimadom Slum development</td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td>Special projects: Slum Development</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td>Government </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td>Yes </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td>Thiruvananthapuram </td>
							</tr>
							<tr>
								<td>State</td>
								<td>Kerala  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td>Consultancy and construction / Construction only / DPR only / PMC</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td></td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td> </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td>    </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td></td>
							</tr>
							<tr>
								<td>Roof</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td></td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr>
								<td>Any other</td>
								<td>        </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>     </td>
							</tr>
							
						</tbody>
						
					</table>                   
				</div>
				
			   <div class="clearfix"> </div>
			</div>	
			
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (1).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (1).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top" >
								<a href="../images/img/1 (2).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (2).jpg" alt="Costford" class="img-responsive" style="height: 236px;">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (3).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (3).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (4).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (4).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (5).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (5).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (6).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (6).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div>
							
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<!-- //Tab-2 -->

					<!-- Tab-3 -->
					<section id="section-bar-3" class="agileits w3layouts">
						<div class="gallery-grids">
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g7.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g7.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g3.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g3.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g9.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g9.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
	
					

			
							<div class="clearfix"></div>
						</div>
					</section>
					<!-- //Tab-3 -->
					<!-- Tab-4 -->
					<section id="section-bar-4" class="agileits w3layouts">
						<div class="gallery-grids">
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g7.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g7.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g3.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g3.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g9.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g9.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
	
					

			
							<div class="clearfix"></div>
						</div>
					</section>
					<!-- //Tab-4 -->
					
				</div><!-- //Content -->
			</div><!-- //Tabs -->

		</div>
	</div>
	<!-- //Portfolio -->




<!-- footer -->
<div class="footer">
		<div class="container">
			
			<div class="agile_footer_copy">
				<div class="w3agile_footer_grids">
					<div class="col-md-4 w3agile_footer_grid">
						<h3>About Us</h3>
						<p style="text-align: justify;">Centre of Science and Technology For Rural Development (COSTFORD) is a Thrissur city-based organisation that gives technological assistance to people in alternative building technology.</p>
					</div>
					<div class="col-md-4 w3agile_footer_grid">
						<h3>Contact Info</h3>
						<ul>
							<li><i class="fa fa-map-marker" aria-hidden="true"></i>The Hamlet, Benedict Nagar,
Nalanchira P.O, <span>Thiruvananthapuram 695015</span></li>
							<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@example.com">costfordtvm@gmail.com</a></li>
							<li><i class="fa fa-phone" aria-hidden="true"></i>0471 - 2530031, 09446540220</li>
						</ul>
					</div>
					<div class="col-md-4 w3agile_footer_grid w3agile_footer_grid1">
						<h3>Navigation</h3>
						<ul>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="#">Architecture</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="#">Costford Centres</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="#">About</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="#">Contact Us</a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<!--<div class="w3_agileits_copy_right_social">
				<div class="col-md-6 agileits_w3layouts_copy_right">
					<p>&copy; 2020. All rights reserved | Design by <a href="http://grameena.org/">Grameena Patana Kendram</a></p>
				</div>
				<div class="col-md-6 w3_agile_copy_right">
					<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
				</div>
				<div class="clearfix"> </div>
			</div>-->
		</div>
	</div>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
 <!-- js -->
<script src="../js/jquery-2.2.3.min.js"></script>
<!-- Gallery-Tab-JavaScript -->
			<script src="../js/cbpFWTabs.js"></script>
			<script>
				(function() {
					[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
						new CBPFWTabs( el );
					});
				})();
			</script>
		<!-- //Gallery-Tab-JavaScript -->


<!-- Swipe-Box-JavaScript -->
			<script src="../js/jquery.swipebox.min.js"></script> 
				<script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
			</script>
		<!-- //Swipe-Box-JavaScript -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Health Plus
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="../images/g9.jpg" alt=" " class="img-responsive" />
						<p>Ut enim ad minima veniam, quis nostrum 
							exercitationem ullam corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi consequatur? Quis autem 
							vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur, vel illum qui 
							dolorem eum fugiat quo voluptas nulla pariatur.
							<i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
								esse quam nihil molestiae consequatur.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="../js/move-top.js"></script>
<script type="text/javascript" src="../js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
			<script src="../js/jarallax.js"></script>
	<script src="../js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>
	
	<script src="../js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>