<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_STP_DIR . PJI_COR_DIR . "utility.php");
require_once(PJI_STP_DIR . PJI_COR_DIR . "admin-utility.php");
$table_main = $db_sfx . "arch_project";
$table_sfx = "project_";
$dyn_folder = PJI_STP_DIR . PJI_IMG_DIR . PJI_APR_DIR;
check_login();
$tabm = 7;
$tab = 10;
$tabl = 24;
if(isset($_REQUEST['id']));
{
    $id = $_REQUEST['id'];
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php include("includes/header1.php");?>
    <title>Costford | Project Edit</title>
</head>
<script>
    function vald()
    {
        var desc = CKEDITOR.instances.desc.getData();
        if(desc == "")
        {
            alert("Please Enter Description");
            return false;
        }
    }
</script>
<!-- <script>
    function destimg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img1').attr('src', e.target.result) .width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
      
       
</script> -->
<script>
    function destimg2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img2').attr('src', e.target.result) .width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
      
       
</script>

<!-- END HEAD -->
<?php
if (isset($_POST['add']) == 'add') 
{  
       if($_FILES['image']['name'])
    {
        $img = $_FILES['image'];
        $max_size = 0;
        $max_file_size = 1024 * 1024 * 8; //8 MB
        $valid_ext = array("jpg", "jpeg", "gif", "png");
        $img_sfx = "project_";
        $img = image_upload($img, $dyn_folder, $img_sfx, $max_size, $valid_ext); //uploading image
        
        $db_data[$table_sfx.'image'] = $img;
    }
    
        if($_FILES['image2']['name'])
    {
        $img = $_FILES['image2'];
        $max_size = 0;
        $max_file_size = 1024 * 1024 * 8; //8 MB
        $valid_ext = array("jpg", "jpeg", "gif", "png");
        $img_sfx = "project_";
        $img = image_upload($img, $dyn_folder, $img_sfx, $max_size, $valid_ext); //uploading image
        
        $db_data[$table_sfx.'frontimage'] = $img;
    }    
        

        $db_data[$table_sfx.'name'] = $_POST['pname'];
        $db_data[$table_sfx.'type'] = $_POST['ptype'];
        $db_data[$table_sfx.'ctype'] = $_POST['ctype'];
        $db_data[$table_sfx.'cname'] = $_POST['cname'];
        $db_data[$table_sfx.'place'] = $_POST['place'];
        $db_data[$table_sfx.'canname'] = $_POST['cancustomer'];
        $db_data[$table_sfx.'locality'] = $_POST['locality'];
        $db_data[$table_sfx.'district'] = $_POST['district'];
        $db_data[$table_sfx.'state'] = $_POST['state'];
        $db_data[$table_sfx.'subcentre'] = $_POST['subcentre'];
        $db_data[$table_sfx.'service'] = $_POST['service'];
        $db_data[$table_sfx.'year'] = $_POST['year'];
        $db_data[$table_sfx.'cost'] = $_POST['cost'];
        $db_data[$table_sfx.'currentcost'] = $_POST['current'];
        $db_data[$table_sfx.'area'] = $_POST['area'];
        $db_data[$table_sfx.'specific'] = $_POST['specifications'];
        $db_data[$table_sfx.'found'] = $_POST['found'];
        $db_data[$table_sfx.'super'] = $_POST['super'];
        $db_data[$table_sfx.'roof'] = $_POST['roof'];
        $db_data[$table_sfx.'doors'] = $_POST['doors'];
        $db_data[$table_sfx.'floor'] = $_POST['floor'];
        $db_data[$table_sfx.'wall'] = $_POST['wall'];
        $db_data[$table_sfx.'any'] = $_POST['anyother'];
        $db_data[$table_sfx.'brief'] = $_POST['write-up'];
        $db_data['status'] = $_POST['status'];
        
        $update_data = update_data($table_main,$db_data,"project_id=$id",true,$conn);
        if ($update_data == 1 ) 
        {
        ?>
        <script type="text/javascript">
        alert('Successfully added'); //sucess , error, info
        setTimeout("window.location = 'arch_project_list.php'", 100);
        </script>
        <?php
    } 
    else
    {
    ?>
        <script type="text/javascript">
        alert( 'Error occured, Try Again.');
        </script>
    <?php
    }
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
        <?php include("includes/header.php");?>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
           
<?php include("includes/sidebar.php");?>
        <!-- BEGIN PAGE -->  
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>portlet Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
         <br/>
         <br/>
            
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN VALIDATION STATES-->
                        <div class="portlet box blue tabbable">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="hidden-480">Edit  Costford Project List</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                        
                                <!-- BEGIN FORM-->
                         <br/><br/>
                        <?php
                            $rows = select_a_row($table_main,"project_id=$id",$conn,true);
                            foreach($rows as $row)
                            {
                                        $id = $row['project_id']; 
                                        $image = $row[$table_sfx.'frontimage'];
                                        $name = $row[$table_sfx.'name'];
                                        $type = $row[$table_sfx.'type'];
                                        $ctype = $row[$table_sfx.'ctype'];
                                        $cname = $row[$table_sfx.'cname'];
                                        $place = $row[$table_sfx.'place'];
                                        $canname = $row[$table_sfx.'canname'];
                                        $locality = $row[$table_sfx.'locality'];
                                        $district = $row[$table_sfx.'district'];
                                        $state= $row[$table_sfx.'state'];
                                        $subcentre = $row[$table_sfx.'subcentre'];
                                        $service = $row[$table_sfx.'service'];
                                        $year = $row[$table_sfx.'year'];
                                        $cost = $row[$table_sfx.'cost'];
                                        $currentcost = $row[$table_sfx.'currentcost'];
                                        $area = $row[$table_sfx.'area'];
                                        $specific = $row[$table_sfx.'specific'];
                                        $found = $row[$table_sfx.'found'];
                                        $super = $row[$table_sfx.'super'];
                                        $roof = $row[$table_sfx.'roof'];
                                        $doors = $row[$table_sfx.'doors'];
                                        $floor = $row[$table_sfx.'floor'];
                                        $wall = $row[$table_sfx.'wall'];
                                        $any = $row[$table_sfx.'any'];
                                        $brief = $row[$table_sfx.'brief'];
                                        $status = $row['status'];
                            }
                            ?>        
    <form action="" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data">

                                    <div class="control-group">
                                        <label class="control-label">Front Image </label>
                                        <div class="controls">
                                         <img src="<?php echo $dyn_folder.$image;?>" height="100" width="100">
                                            <input type="file" name="image2" id="image2" class="span5 m-wrap" onchange="destimg2(this);"/> <img src="" id="img2">
                                        </div>
                                    </div>  
                                   
                                    <div class="control-group">
                                        <label class="control-label">Project Name<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="pname" class="span10 m-wrap" required="required" value="<?php echo $name;?>"/>
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Project Type<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="type" name="ptype"  class="span5 m-wrap" required>
                                           
                                           <?php if($type == "Residential" )
                                           {
                                                ?>
                                                  <option value="Residential">Residental</option>
                                                  <option value="Private">Private</option>
                                                  <option value="Institutional">Institutional</option>
                                                  <option value="Social_housing">Social Housing</option>
                                                <?php
                                            } ?>
                                             <?php if($type == "Private")
                                           {
                                                ?>
                                                  <option value="Residential">Residental</option>
                                                  <option value="Private">Private</option>
                                                  <option value="Institutional">Institutional</option>
                                                  <option value="Social_housing">Social Housing</option>
                                                    
                                                <?php
                                            } ?>
                                             <?php if($type == "Institutional")
                                           {
                                                ?>
                                                  <option value="Residential">Residental</option>
                                                  <option value="Private">Private</option>
                                                  <option value="Institutional">Institutional</option>
                                                  <option value="Social_housing">Social Housing</option>
                                                    
                                                <?php
                                            } ?>
                                             <?php if($type == "Social_housing")
                                           {
                                                ?>
                                                  <option value="Residential">Residental</option>
                                                  <option value="Private">Private</option>
                                                  <option value="Institutional">Institutional</option>
                                                  <option value="Social_housing">Social Housing</option>
                                                    
                                                <?php
                                            } ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                     <div class="control-group">
                                        <label class="control-label">Customer Type<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="ctype" class="span10 m-wrap" required="required" value="<?php echo $ctype;?>"/>
                                            
                                        </div>
                                    </div>
                                    
                                     <div class="control-group">
                                        <label class="control-label">Customer Name<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="cname" class="span10 m-wrap" required="required" value="<?php echo $cname;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Centre Place<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="place" class="span10 m-wrap" required="required" value="<?php echo $place;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Can Customer name be published<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="cancustomer" class="span10 m-wrap" required="required" value="<?php echo $canname;?>"/>
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Locality<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="locality" class="span10 m-wrap" required="required" value="<?php echo $locality;?>"/>
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">District<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="district" class="span10 m-wrap" required="required" value="<?php echo $district;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">State<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="state" class="span10 m-wrap" required="required" value="<?php echo $state;?>"/>
                                            
                                        </div>
                                    </div>

                                     <div class="control-group">
                                        <label class="control-label">Sub Centre name<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="subcentre" class="span10 m-wrap" required="required" value="<?php echo $subcentre;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Type of service provided<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="service" class="span10 m-wrap" required="required" value="<?php echo $service;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Year Of construction<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="year" class="span10 m-wrap" required="required" value="<?php echo $year;?>"/>
                                            
                                        </div>
                                    </div> 

                                    <div class="control-group">
                                        <label class="control-label">Cost of construction<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="cost" class="span10 m-wrap" required="required" value="<?php echo $cost;?>"/>
                                            
                                        </div>
                                    </div> 
                                    <div class="control-group">
                                        <label class="control-label">Current Cost rate<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="current" class="span10 m-wrap" required="required" value="<?php echo $currentcost;?>"/>
                                            
                                        </div>
                                    </div> 
                                      <div class="control-group">
                                        <label class="control-label">Total build-up area<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="area" class="span10 m-wrap" id="txt_number" maxlength="70"  placeholder="Number" onkeypress="return isNumberKey(event)" required="required" value="<?php echo $area;?>"/>
                                            
                                        </div>
                                    </div> 
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                            <div class="control-group">
                                                <label class="control-label" >Brief Specification</label>
                                                <div class="controls">
                                                    <textarea class="span12 ckeditor m-wrap" id="specifications" name= "specifications" rows="6"  ><?php echo $specific;?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Foundation<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="found" class="span10 m-wrap" required="required" value="<?php echo $found;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">SuperStructure<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="super" class="span10 m-wrap" required="required" value="<?php echo $super;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Roof<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="roof" class="span10 m-wrap" required="required" value="<?php echo $roof;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Doors And Windows<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="doors" class="span10 m-wrap" required="required" value="<?php echo $doors;?>"/>
                                            
                                        </div>
                                    </div>
                                     <div class="control-group">
                                        <label class="control-label">Floor Finish<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="floor" class="span10 m-wrap" required="required" value="<?php echo $floor;?>"/>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Wall Finish<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="wall" class="span10 m-wrap" required="required" value="<?php echo $wall;?>"/>
                                            
                                        </div>
                                    </div>
                                     <div class="control-group">
                                        <label class="control-label">Any other</label>
                                        <div class="controls">
                                            <input type="text" name="anyother" class="span10 m-wrap"  value="<?php echo $any;?>"/>
                                            
                                        </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12 ">
                                            <div class="control-group">
                                                <label class="control-label" >A brief write-up<span class="required">*</span></label>
                                                <div class="controls">
                                                    <textarea class="span12 ckeditor m-wrap" id="write-up" name= "write-up" rows="6" required="required" ><?php echo $brief;?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Status<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="status" name="status"  class="span5 m-wrap" required>
                                           
                                           <?php if($status == 1)
                                           {
                                                ?>
                                                  <option value="1">Published</option>
                                                  <option value="0">Unpublished</option>
                                                <?php
                                            } ?>
                                             <?php if($status == 0)
                                           {
                                                ?>
                                                  <option value="0">Unpublished</option>
                                                  <option value="1">Published</option>
                                                    
                                                <?php
                                            } ?>
                                           
                                            </select>
                                        </div>
                                    </div>
                               
                                    <div class="form-actions">
                                        <button type="submit" id="add" name="add" value="add" class="btn blue" onclick="return vald()"><i class="icon-ok"></i>Save</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
       
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
<?php include("includes/footer.php");?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <script type="text/javascript" src="../assets/plugins/ckeditor/ckeditor.js"></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/scripts/app.js"></script>
    <script src="../assets/scripts/form-components.js"></script>  

    <script src="../assets/scripts/form-validation.js"></script>    
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- Script for only for Number -->
    <script>
        function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;
         return true;
        }
    </script>
    <!-- Number Script Ends -->
    <script>
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           FormComponents.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>