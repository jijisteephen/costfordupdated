<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_STP_DIR . PJI_COR_DIR . "utility.php");
require_once(PJI_STP_DIR . PJI_COR_DIR . "admin-utility.php");
$table_main = $db_sfx . "testimonials";
$table_sfx = "testimonials_";
$dyn_folder = PJI_STP_DIR . PJI_IMG_DIR . PJI_HSR_DIR;
check_login();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php include("includes/header1.php");
$tabm = 7;
$tab = 0;
$tabl = 14;
	 ?>
	<title>Anchal  | Testimonials List</title>
</head>
<script>
	function del()
	{
		return confirm("Are You Sure You Want To Delete!");
	}
</script>
	 <?php 
       if(isset($_REQUEST['delid']))
         {
            $del_id=$_REQUEST['delid'];
            $data['b_active']='0';                     
            $where_del="testimonials_id ='".$del_id."'";
            update_data($table_main,$data,$where_del,$update=true,$conn);
        }
      ?>
	
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<?php include("includes/header.php"); ?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
     <?php 
     include("includes/sidebar.php");  ?>
     <?php 
     $where="b_active='1'";
     $rows = select_all_rows($table_main, $where, $conn, true);
     $slno=1;  
     ?>

		<!-- BEGIN PAGE -->  
		<div class="page-content">
			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<BR><BR>
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
                 <div class="span12">
             	 <!-- BEGIN BORDERED TABLE PORTLET-->
             	 <!-- SEARCH START-->
             	
				 <!-- SEARCH END-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">Service List</div>
							</div>
							<div class="portlet-body no-more-tables">
									<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead
										<tr>
											<th>Sl&nbsp;No:</th>
											<th>Name</th>
											<th>Place</th>
											<th>Testimonials</th>
											<th>Edit</th>
											<th>Status</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody>
									
										<?php
									if(isset($rows))
									{
									foreach($rows as $row)
								    {
								     	$id = $row['testimonials_id'];
								     	$name = $row[$table_sfx.'name'];
								     	$content =substr(html_entity_decode($row[$table_sfx.'content'],ENT_QUOTES),0,50);
								     	$place = $row[$table_sfx.'place'];
								     	$status = $row['status'];
								     	?>
										<tr>
											<td data-title="Sl No:"><?php echo $slno++;?></td>
											<td data-title="name"><?php echo ucfirst($name);?></td>
											<td data-title="place"><?php echo ucfirst($place);?></td>
											<td data-title="content"><?php echo ucfirst($content."...");?></td>
											<td data-title="Edit"><a href="testimonials_edit.php?id=<?php echo $id;?>">Edit</a></td>


											 <?php if($status == 1)
                                           {
                                                ?>
                                                  <td data-title="Delete" style="color: green">Published</td>
                                                <?php
                                            } ?>
                                             <?php if($status == 0)
                                           {
                                                ?>
                                                 <td data-title="Delete" style="color: red">UnPublished</td>
                                                    
                                                <?php
                                            } ?>
											<td data-title="Delete"><a href="testimonials_list.php?delid=<?php echo $id;?>" onclick ="return del()">Delete</a></td>
										</tr>
								<?php
								    }
								}
								    ?>
								   
								    </tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
		<?php include("includes/footer.php");
		?>
	<script src="../assets/scripts/app.js"></script>
	<script src="../assets/scripts/table-editable.js"></script>    
	<script>
		jQuery(document).ready(function() {       
		   App.init();
		   TableEditable.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>