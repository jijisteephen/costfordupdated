<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_STP_DIR . PJI_COR_DIR . "utility.php");
require_once(PJI_STP_DIR . PJI_COR_DIR . "admin-utility.php");
$table_main = $db_sfx . "arch_build";
$table_sfx = "build_";
$dyn_folder = PJI_STP_DIR . PJI_IMG_DIR . PJI_ABU_DIR;
check_login();
$tabm = 7;
$tab = 13;
$tabl = 30;
if(isset($_REQUEST['id']));
{
    $id = $_REQUEST['id'];
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php include("includes/header1.php");?>
    <title>Costford | Material And Technology Edit</title>
</head>
<script>
    function vald()
    {
        var desc = CKEDITOR.instances.desc.getData();
        if(desc == "")
        {
            alert("Please Enter Description");
            return false;
        }
    }
</script>
<script>
    function destimg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img1').attr('src', e.target.result) .width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
      
       
</script>

<!-- END HEAD -->
<?php
if (isset($_POST['add']) == 'add') 
{  
       if($_FILES['image']['name'])
    {
        $img = $_FILES['image'];
        $max_size = 0;
        $max_file_size = 1024 * 1024 * 8; //8 MB
        $valid_ext = array("jpg", "jpeg", "gif", "png");
        $img_sfx = "build_";
        $img = image_upload($img, $dyn_folder, $img_sfx, $max_size, $valid_ext); //uploading image
        
        $db_data[$table_sfx.'image'] = $img;
    }
      
        
        
        $db_data[$table_sfx.'title'] = $_POST['title'];
        $db_data[$table_sfx.'details'] = $_POST['details'];
        $db_data['status'] = $_POST['status'];
        $update_data = update_data($table_main,$db_data,"build_id=$id",true,$conn);
        if ($update_data == 1 ) 
        {
        ?>
        <script type="text/javascript">
        alert('Successfully added'); //sucess , error, info
        setTimeout("window.location = 'arch_build_list.php'", 100);
        </script>
        <?php
    } 
    else
    {
    ?>
        <script type="text/javascript">
        alert( 'Error occured, Try Again.');
        </script>
    <?php
    }
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
        <?php include("includes/header.php");?>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
           
<?php include("includes/sidebar.php");?>
        <!-- BEGIN PAGE -->  
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>portlet Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
         <br/>
         <br/>
            
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN VALIDATION STATES-->
                        <div class="portlet box blue tabbable">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="hidden-480">Edit  Build With Us List</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                        
                                <!-- BEGIN FORM-->
                         <br/><br/>
                        <?php
                            $rows = select_a_row($table_main,"build_id=$id",$conn,true);
                            foreach($rows as $row)
                            {
                                        $id = $row['build_id'];
                                        $title = $row[$table_sfx.'title'];
                                        $image = $row[$table_sfx.'image'];
                                        $details = $row[$table_sfx.'details'];
                                        $status = $row['status'];
                            }
                            ?>        
    <form action="" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data">

                                      
                                   
                                    <div class="control-group">
                                        <label class="control-label">Title<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="title" class="span10 m-wrap" required="required" value="<?php echo $title;?>"/>
                                            
                                        </div>
                                    </div>
                                    

                                    <div class="control-group">
                                        <label class="control-label"> Image <span class="required">*</span></label>
                                        <div class="controls">
                                         <img src="<?php echo $dyn_folder.$image;?>" height="100" width="100">
                                            <input type="file" name="image" id="image" class="span5 m-wrap" onchange="destimg(this);"/> <img src="" id="img1">
                                        </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                            <div class="control-group">
                                                <label class="control-label" >Description<span class="required">*</span></label>
                                                <div class="controls">
                                                    <textarea class="span12 ckeditor m-wrap" id="details" name= "details" rows="6" required="required" ><?php echo $details;?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    

                                    <div class="control-group">
                                        <label class="control-label">Status<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="status" name="status"  class="span5 m-wrap" required>
                                           
                                           <?php if($status == 1)
                                           {
                                                ?>
                                                  <option value="1">Published</option>
                                                  <option value="0">Unpublished</option>
                                                <?php
                                            } ?>
                                             <?php if($status == 0)
                                           {
                                                ?>
                                                  <option value="0">Unpublished</option>
                                                  <option value="1">Published</option>
                                                    
                                                <?php
                                            } ?>
                                           
                                            </select>
                                        </div>
                                    </div>
                               
                                    <div class="form-actions">
                                        <button type="submit" id="add" name="add" value="add" class="btn blue" onclick="return vald()"><i class="icon-ok"></i>Save</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
       
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
<?php include("includes/footer.php");?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <script type="text/javascript" src="../assets/plugins/ckeditor/ckeditor.js"></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/scripts/app.js"></script>
    <script src="../assets/scripts/form-components.js"></script>  

    <script src="../assets/scripts/form-validation.js"></script>    
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           FormComponents.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>