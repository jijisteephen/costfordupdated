
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<ul class="page-sidebar-menu">
				<br/><br/><br/>
				<br/>
				<li class="start" <?php if ($tabm==1) {?> class="active "<?php } ?>>
					<a href="home.php">
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				<!-- Home Tab Start -->
				<li <?php if ($tabm==1) {?> class="active "<?php } ?>>
					<a href="">
					<span class="title">Home</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
					<li <?php if ($tab==0) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Slider Images</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 1) { ?> class="active" <?php } ?>>
									<a href="home_slider_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 2) { ?> class="active" <?php } ?>>
									<a href="home_slider_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<!-- <li <?php if ($tab==1) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Services</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 3) { ?> class="active" <?php } ?>>
									<a href="home_service_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 4) { ?> class="active" <?php } ?>>
									<a href="home_service_list.php">
									List</a>
								</li>
							</ul>
						</li> -->
						
						
					</ul>
				</li>
				<!-- Home Tab End -->
				<!-- About Tab Start -->
				<li <?php if ($tabm==2) {?> class="active "<?php } ?>>
					<a href="">
					<span class="title">About</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
					<li <?php if ($tab==2) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Leaders</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 3) { ?> class="active" <?php } ?>>
									<a href="about_leaders_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 4) { ?> class="active" <?php } ?>>
									<a href="about_leaders_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==3) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">History</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 5) { ?> class="active" <?php } ?>>
									<a href="about_history_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 6) { ?> class="active" <?php } ?>>
									<a href="about_history_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==4) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Approach</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 7) { ?> class="active" <?php } ?>>
									<a href="about_approach_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 8) { ?> class="active" <?php } ?>>
									<a href="about_approach_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==5) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Vision / Mission</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 9) { ?> class="active" <?php } ?>>
									<a href="about_vision_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 10) { ?> class="active" <?php } ?>>
									<a href="about_vision_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==6) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Governing Body</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 11) { ?> class="active" <?php } ?>>
									<a href="about_gover_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 12) { ?> class="active" <?php } ?>>
									<a href="about_gover_list.php">
									List</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<!-- About Tab End -->
				<!-- Activities Tab Start -->
				<li <?php if ($tabm==3) {?> class="active "<?php } ?>>
					<a href="">
					<span class="title">Activities</span>
					<span class="arrow "></span>
					</a>
					<!-- <ul class="sub-menu">
					<li <?php if ($tab==7) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Activities</span>
							<span class="arrow "></span>
							</a> -->
							<ul class="sub-menu">
								<li <?php if($tabl == 13) { ?> class="active" <?php } ?>>
									<a href="inner_activities_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 14) { ?> class="active" <?php } ?>>
									<a href="inner_activities_list.php">
									List</a>
								</li>
							</ul>
						<!-- </li>
						
					</ul> -->
				</li>
				<!-- Activities Tab End -->
				<!-- Costford Centre Tab Start -->
				<li <?php if ($tabm==4) {?> class="active "<?php } ?>>
					<a href="">
					<span class="title">Centre</span>
					<span class="arrow "></span>
					</a>
					<!-- <ul class="sub-menu">
					<li <?php if ($tab==7) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Activities</span>
							<span class="arrow "></span>
							</a> -->
							<ul class="sub-menu">
								<li <?php if($tabl == 15) { ?> class="active" <?php } ?>>
									<a href="inner_centres_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 16) { ?> class="active" <?php } ?>>
									<a href="inner_centres_list.php">
									List</a>
								</li>
							</ul>
						<!-- </li>
						
					</ul> -->
				</li>
				<!-- Costford Centres Tab End -->
				<!-- Publications Tab Start -->
				<li <?php if ($tabm==5) {?> class="active "<?php } ?>>
					<a href="">
					<span class="title">Publications</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
					<!-- <li <?php if ($tab==8) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Laurie Baker</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 15) { ?> class="active" <?php } ?>>
									<a href="inner_laurie_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 16) { ?> class="active" <?php } ?>>
									<a href="inner_laurie_list.php">
									List</a>
								</li>
							</ul>
						</li> -->
						<li <?php if ($tab==9) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Other</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 17) { ?> class="active" <?php } ?>>
									<a href="inner_other_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 18) { ?> class="active" <?php } ?>>
									<a href="inner_other_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==10) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">News & Events</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 19) { ?> class="active" <?php } ?>>
									<a href="inner_news_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 20) { ?> class="active" <?php } ?>>
									<a href="inner_news_list.php">
									List</a>
								</li>
							</ul>
						</li>
						
					</ul>
				</li>


				<!-- Publications Tab End -->
				<!-- Gallery Tab Start -->
				<li <?php if ($tabm==6) {?> class="active "<?php } ?>>
					<a href="">
					<span class="title">Gallery</span>
					<span class="arrow "></span>
					</a>
					<!-- <ul class="sub-menu">
					<li <?php if ($tab==7) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Activities</span>
							<span class="arrow "></span>
							</a> -->
							<ul class="sub-menu">
								<li <?php if($tabl == 21) { ?> class="active" <?php } ?>>
									<a href="inner_gallery_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 22) { ?> class="active" <?php } ?>>
									<a href="inner_gallery_list.php">
									List</a>
								</li>
							</ul>
						<!-- </li>
						
					</ul> -->
				</li>
				<!-- Gallery Tab End -->
                <!-- Architecture Tab End -->
				<li <?php if ($tabm==7) {?> class="active "<?php } ?>>
					<a href="">
					<span class="title">Architecture</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
					<li <?php if ($tab==10) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Projects</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 23) { ?> class="active" <?php } ?>>
									<a href="arch_project_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 24) { ?> class="active" <?php } ?>>
									<a href="arch_project_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==11) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Approach</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 25) { ?> class="active" <?php } ?>>
									<a href="arch_approach_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 26) { ?> class="active" <?php } ?>>
									<a href="arch_approach_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==12) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Material & Technology</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 27) { ?> class="active" <?php } ?>>
									<a href="arch_material_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 28) { ?> class="active" <?php } ?>>
									<a href="arch_material_list.php">
									List</a>
								</li>
							</ul>
						</li>
						<li <?php if ($tab==13) {?> class="start active "<?php } ?>>
							<a href="">
							<span class="title">Build with us</span>
							<span class="arrow "></span>
							</a>
							<ul class="sub-menu">
								<li <?php if($tabl == 29) { ?> class="active" <?php } ?>>
									<a href="arch_build_add.php">
									Add</a>
								</li>
								<li <?php if($tabl == 30) { ?> class="active" <?php } ?>>
									<a href="arch_build_list.php">
									List</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<!-- Architecture Tab Ends -->
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>