<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_STP_DIR . PJI_COR_DIR . "utility.php");
require_once(PJI_STP_DIR . PJI_COR_DIR . "admin-utility.php");
$table_main = $db_sfx . "arch_project";
$table_sfx = "project_";
$dyn_folder = PJI_STP_DIR . PJI_IMG_DIR . PJI_ARA_DIR;
check_login();
$tabm = 7;
$tab = 10;
$tabl = 23;
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php include("includes/header1.php");?>
    <title>Costford | Add Project</title>
</head>
<!-- <script>
    function vald()
    {
        var desc = CKEDITOR.instances.desc.getData();
        if(desc == "")
        {
            alert("Please Enter Description");
            return false;
        }
    }
</script> -->
<!-- <script>
    function newimg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#disp').attr('src', e.target.result) .width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
</script> -->
<script>
    function newimg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#disp').attr('src', e.target.result) .width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
</script>
<script>
    function newimg2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#disp2').attr('src', e.target.result) .width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
</script>


<!-- END HEAD -->
<?php
if (isset($_POST['add']) == 'add') 
{  
       if($_FILES['image']['name'])
    {
        $img = $_FILES['image'];
        $max_size = 0;
        $max_file_size = 1024 * 1024 * 8; //8 MB
        $valid_ext = array("jpg", "jpeg", "gif", "png");
        $img_sfx = "project_";
        $img = image_upload($img, $dyn_folder, $img_sfx, $max_size, $valid_ext); //uploading image
        
        $db_data[$table_sfx.'image'] = $img;
    }
      if($_FILES['image2']['name'])
    {
        $img = $_FILES['image2'];
        $max_size = 0;
        $max_file_size = 1024 * 1024 * 8; //8 MB
        $valid_ext = array("jpg", "jpeg", "gif", "png");
        $img_sfx = "lea_";
        $img = image_upload($img, $dyn_folder, $img_sfx, $max_size, $valid_ext); //uploading image
        
        $db_data[$table_sfx.'frontimage'] = $img;
    }
       
        $db_data['b_active'] = 1;
        $db_data[$table_sfx.'name'] = $_POST['pname'];
        $db_data[$table_sfx.'type'] = $_POST['ptype'];
        $db_data[$table_sfx.'ctype'] = $_POST['ctype'];
        $db_data[$table_sfx.'cname'] = $_POST['cname'];
        $db_data[$table_sfx.'place'] = $_POST['place'];
        $db_data[$table_sfx.'canname'] = $_POST['cancustomer'];
        $db_data[$table_sfx.'locality'] = $_POST['locality'];
        $db_data[$table_sfx.'district'] = $_POST['district'];
        $db_data[$table_sfx.'state'] = $_POST['state'];
        $db_data[$table_sfx.'subcentre'] = $_POST['subcentre'];
        $db_data[$table_sfx.'service'] = $_POST['service'];
        $db_data[$table_sfx.'year'] = $_POST['year'];
        $db_data[$table_sfx.'cost'] = $_POST['cost'];
        $db_data[$table_sfx.'currentcost'] = $_POST['current'];
        $db_data[$table_sfx.'area'] = $_POST['area'];
        $db_data[$table_sfx.'specific'] = $_POST['specifications'];
        $db_data[$table_sfx.'found'] = $_POST['found'];
        $db_data[$table_sfx.'super'] = $_POST['super'];
        $db_data[$table_sfx.'roof'] = $_POST['roof'];
        $db_data[$table_sfx.'doors'] = $_POST['doors'];
        $db_data[$table_sfx.'floor'] = $_POST['floor'];
        $db_data[$table_sfx.'wall'] = $_POST['wall'];
        $db_data[$table_sfx.'any'] = $_POST['anyother'];
        $db_data[$table_sfx.'brief'] = $_POST['write-up'];
        $db_data['status'] = $_POST['status'];
        $add_data = insert_data($table_main,$db_data,true,$conn); 
        // $add_data;
        // exit();
        if ($add_data == 1 ) 
        {
        ?>
        <script type="text/javascript">
        alert('Successfully saved'); //sucess , error, info
        setTimeout("window.location = 'arch_project_list.php'", 100);
        </script>
        <?php
    } 
    else
    {
   
    ?>
        <script type="text/javascript">
        alert( 'Error occured, Try Again.');
        </script>
    <?php
    }
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
        <?php include("includes/header.php");?>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
           
<?php include("includes/sidebar.php");?>
        <!-- BEGIN PAGE -->  
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>portlet Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
         <br/>
         <br/>
            
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN VALIDATION STATES-->
                        <div class="portlet box blue tabbable">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="hidden-480">Add Costford Project</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                        
                                <!-- BEGIN FORM-->
                         <br/><br/>
                  
    <form action="" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data">
                               
                                   <div class="control-group">
                                        <label class="control-label">Front  Image </label>
                                        <div class="controls">
                                            <input type="file" name="image2" id="image2" class="span5 m-wrap" onchange="newimg2(this);"/> <img src="" id="disp2">
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <label class="control-label">Project Name<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="pname" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Project Type<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="type" name="ptype"  class="span5 m-wrap" required>
                                            <option value="">---Select---</option>
                                            <option value="Residential">Residental</option>
                                            <option value="Private">Private</option>
                                            <option value="Institutional">Institutional</option>
                                            <option value="Social_housing">Social Housing</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Centre Place<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="place" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Customer Type<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="ctype" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Customer Name<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="cname" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Can Customer name be published<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="cancustomer" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Locality<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="locality" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">District<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="district" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">State<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="state" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Sub Centre name<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="subcentre" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Type of service provided<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="service" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Year Of construction<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="year" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Cost of construction<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="cost" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Cost at current rates<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="current" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Total build-up area<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="number" id="txt_number" maxlength="70" name="area" class="span10 m-wrap" required="required" placeholder="Number" onkeypress="return isNumberKey(event)">
                                            
                                        </div>
                                    </div>

                                     <!-- <div class="control-group">
                                        <label class="control-label">Brief specifications<span class="required">*</span></label>
                                        <div class="controls">
                                             <textarea class="span12 ckeditor m-wrap" id="specifications" name= "specifications" rows="6" ></textarea>
                                            
                                        </div>
                                    </div> -->

                                    <div class="control-group">
                                        <label class="control-label">Foundation<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="found" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">SuperStructure<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="super" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Roof<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="roof" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Doors and Windows<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="doors" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Floor Finish<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="floor" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Wall Finish<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="wall" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Any other</label>
                                        <div class="controls">
                                            <input type="text" name="anyother" class="span10 m-wrap" >
                                            
                                        </div>
                                    </div>
                                

                                    <div class="control-group">
                                        <label class="control-label">A brie write-up<span class="required">*</span></label>
                                        <div class="controls">
                                             <textarea class="span12 ckeditor m-wrap" id="write-up" name= "write-up" rows="6" ></textarea>
                                            
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <label class="control-label">Status<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="status" name="status"  class="span5 m-wrap" required>
                                            <option value="">---Select---</option>
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                            </select>
                                        </div>
                                    </div>
                               
                                    <div class="form-actions">
                                        <button type="submit" id="add" name="add" value="add" class="btn blue" onclick="return vald()"><i class="icon-ok"></i>Save</button>
                                        
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
       
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
<?php include("includes/footer.php");?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <script type="text/javascript" src="../assets/plugins/ckeditor/ckeditor.js"></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/scripts/app.js"></script>
    <script src="../assets/scripts/form-components.js"></script>  

    <script src="../assets/scripts/form-validation.js"></script>    
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
       function isNumberKey(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode != 46 && charCode > 31
         && (charCode < 48 || charCode > 57))
         return false;
         return true;
        }
    </script>
    <script>
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           FormComponents.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>