<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_STP_DIR . PJI_COR_DIR . "utility.php");
require_once(PJI_STP_DIR . PJI_COR_DIR . "admin-utility.php");
$table_main = $db_sfx . "inner_centre";
$table_sfx = "centre_";
$dyn_folder = PJI_STP_DIR . PJI_IMG_DIR . PJI_APP_DIR;
check_login();
$tabm = 4;
$tab = 0;
$tabl = 15;
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php include("includes/header1.php");?>
    <title>Costford | Add  Costford Centres</title>
</head>
<!-- <script>
    function vald()
    {
        var desc = CKEDITOR.instances.desc.getData();
        if(desc == "")
        {
            alert("Please Enter Description");
            return false;
        }
    }
</script> -->
<script>
    function newimg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#disp').attr('src', e.target.result) .width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
</script>

<!-- END HEAD -->
<?php
if (isset($_POST['add']) == 'add') 
{  
    //  if($_FILES['image']['name'])
    // {
    //     $img = $_FILES['image'];
    //     $max_size = 0;
    //     $max_file_size = 1024 * 1024 * 8; //8 MB
    //     $valid_ext = array("jpg", "jpeg", "gif", "png");
    //     $img_sfx = "approach_";
    //     $img = image_upload($img, $dyn_folder, $img_sfx, $max_size, $valid_ext); //uploading image
        
    //     $db_data[$table_sfx.'image'] = $img;
    // }
       
        $db_data['b_active'] = 1;
        $db_data[$table_sfx.'place'] = $_POST['place'];
        $db_data[$table_sfx.'address'] = $_POST['address'];
        $db_data[$table_sfx.'phone'] = $_POST['phone'];
        $db_data[$table_sfx.'fax'] = $_POST['fax'];
         $db_data[$table_sfx.'email'] = $_POST['email'];
        $db_data[$table_sfx.'type'] = $_POST['type'];
        $db_data['status'] = $_POST['status'];
        $add_data = insert_data($table_main,$db_data,true,$conn); 
        // $add_data;
        // exit();
        if ($add_data == 1 ) 
        {
        ?>
        <script type="text/javascript">
        alert('Successfully saved'); //sucess , error, info
        setTimeout("window.location = 'inner_centres_list.php'", 100);
        </script>
        <?php
    } 
    else
    {
   
    ?>
        <script type="text/javascript">
        alert( 'Error occured, Try Again.');
        </script>
    <?php
    }
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
        <?php include("includes/header.php");?>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
           
<?php include("includes/sidebar.php");?>
        <!-- BEGIN PAGE -->  
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>portlet Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
         <br/>
         <br/>
            
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN VALIDATION STATES-->
                        <div class="portlet box blue tabbable">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="hidden-480">Add Costford Centres</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                        
                                <!-- BEGIN FORM-->
                         <br/><br/>
                  
    <form action="" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data">
                               
                                    <div class="control-group">
                                        <label class="control-label">Centre Type<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="type" name="type"  class="span5 m-wrap" required>
                                            <option value="">---Select---</option>
                                            <option value="main">Main</option>
                                            <option value="branch">Sub- branch</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label">Place<span class="required">*</span></label>
                                        <div class="controls">
                                              <input type="text" name="place" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>
                                 
                                    <div class="control-group">
                                        <label class="control-label">Address<span class="required">*</span></label>
                                        <div class="controls">
                                              <input type="text" name="address" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Phone number<span class="required">*</span></label>
                                        <div class="controls">
                                              <input type="text" name="phone" class="span10 m-wrap" required="required">
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label">Fax<span class="required">*</span></label>
                                        <div class="controls">
                                              <input type="text" name="fax" class="span10 m-wrap" >
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label">Email<span class="required">*</span></label>
                                        <div class="controls">
                                              <input type="text" name="email" class="span10 m-wrap" >
                                            
                                        </div>
                                    </div>

                                   

                                    <div class="control-group">
                                        <label class="control-label">Status<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="status" name="status"  class="span5 m-wrap" required>
                                            <option value="">---Select---</option>
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                            </select>
                                        </div>
                                    </div>
                               
                                    <div class="form-actions">
                                        <button type="submit" id="add" name="add" value="add" class="btn blue" onclick="return vald()"><i class="icon-ok"></i>Save</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
       
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
<?php include("includes/footer.php");?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <script type="text/javascript" src="../assets/plugins/ckeditor/ckeditor.js"></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/scripts/app.js"></script>
    <script src="../assets/scripts/form-components.js"></script>  

    <script src="../assets/scripts/form-validation.js"></script>    
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           FormComponents.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>