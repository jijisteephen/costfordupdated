<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_STP_DIR . PJI_COR_DIR . "utility.php");
require_once(PJI_STP_DIR . PJI_COR_DIR . "admin-utility.php");
$table_main = $db_sfx . "arch_project";
// $table_main1 = $db_sfx . "project_image";
$table_sfx = "project_";
$dyn_folder = PJI_STP_DIR . PJI_IMG_DIR . PJI_ARA_DIR;
check_login();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php include("includes/header1.php");
$tabm = 7;
$tab = 10;
$tabl = 24;
	 ?>
	<title>Costford  | Add Project</title>
</head>
<script>
	function del()
	{
		return confirm("Are You Sure You Want To Delete!");
	}
</script>
	 <?php 
       if(isset($_REQUEST['delid']))
         {
            $del_id=$_REQUEST['delid'];
            $data['b_active']='0';                     
            $where_del=" project_id ='".$del_id."'";
            update_data($table_main,$data,$where_del,$update=true,$conn);
        }
      ?>
	
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<?php include("includes/header.php"); ?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
     <?php 
     include("includes/sidebar.php");  ?>
     <?php 
     $where="b_active='1'";
     $rows = select_all_rows($table_main, $where, $conn, true);
     $slno=1;  
     ?>

		<!-- BEGIN PAGE -->  
		<div class="page-content">
			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<BR><BR>
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
                 <div class="span12">
             	 <!-- BEGIN BORDERED TABLE PORTLET-->
             	 <!-- SEARCH START-->
             	
				 <!-- SEARCH END-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">Add Costford Project</div>
							</div>
							<div class="portlet-body no-more-tables">
									<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>Sl&nbsp;No:</th>
											<th>Image</th>
											<th>Project Name</th>
											<th>Project Type</th>
											<th>Customer type</th>
											<th>Customer name</th>
											<th>Published</th>
											<th>locality</th>
											<th>District</th>
											<!-- <th>State</th> -->
											<!-- <th>Sub centre</th>
											<th>Service</th>
											<th>Year</th>
											<th>Cost</th>
											<th>Current cost</th>
											<th>Area</th>
											<th>Specifications</th>
											<th>Foundation</th>
											<th>Superstructure</th>
											<th>Roof</th>
											<th>Doors</th>
											<th>Floor</th>
											<th>Any others</th>
											<th>Write-up</th> -->
											<th>Edit</th>
											<th>Image add</th>
											<th>Status</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody>
									
										<?php
									if(isset($rows))
									{
									foreach($rows as $row)
								    {
								     	$id = $row['project_id']; 
								     	$image = $row[$table_sfx.'frontimage'];
								     	$name = $row[$table_sfx.'name'];
								     	$type = $row[$table_sfx.'type'];
								     	$ctype = $row[$table_sfx.'ctype'];
								     	$cname = $row[$table_sfx.'cname'];
								     	$canname = $row[$table_sfx.'canname'];
								     	$locality = $row[$table_sfx.'locality'];
								     	$district = $row[$table_sfx.'district'];
								     	// $state= $row[$table_sfx.'state'];
								     	// $subcentre = $row[$table_sfx.'subcentre'];
								     	// $service = $row[$table_sfx.'service'];
								     	// $year = $row[$table_sfx.'year'];
								     	// $cost = $row[$table_sfx.'cost'];
								     	// $currentcost = $row[$table_sfx.'currentcost'];
								     	// $area = $row[$table_sfx.'area'];
								     	// $specific = $row[$table_sfx.'specific'];
								     	// $found = $row[$table_sfx.'found'];
								     	// $super = $row[$table_sfx.'super'];
								     	// $roof = $row[$table_sfx.'roof'];
								     	// $doors = $row[$table_sfx.'doors'];
								     	// $floor = $row[$table_sfx.'floor'];
								     	// $any = $row[$table_sfx.'any'];
								     	// $brief = $row[$table_sfx.'brief'];
								     	$status = $row['status'];
								     	?>
										<tr>
											<td data-title="Sl No:"><?php echo $slno++;?></td>
											<td data-title="Image"><img src="<?php echo ($dyn_folder.$image);?>" height="100" width="100"></td>
											<td data-title="Destination"><?php echo ucfirst($name);?></td>
											<td data-title="Destination"><?php echo ucfirst($type);?></td>
											<td data-title="Destination"><?php echo ucfirst($ctype);?></td>
											<td data-title="Destination"><?php echo ucfirst($cname);?></td>
										    <td data-title="Destination"><?php echo ucfirst($canname);?></td>
										    <td data-title="Destination"><?php echo ucfirst($locality);?></td>
										    <td data-title="Destination"><?php echo ucfirst($district);?></td>
										    <!-- <td data-title="Destination"><?php echo ucfirst($state);?></td> -->
											<!-- <td data-title="Destination"><?php echo ucfirst($subcentre);?></td>
											<td data-title="Destination"><?php echo ucfirst($service);?></td>
										    <td data-title="Destination"><?php echo ucfirst($year);?></td>
										    <td data-title="Destination"><?php echo ucfirst($cost);?></td>
										    <td data-title="Destination"><?php echo ucfirst($currentcost);?></td>
										    <td data-title="Destination"><?php echo ucfirst($area);?></td>
										    <td data-title="Destination"><?php echo ucfirst($specific);?></td>
										    <td data-title="Destination"><?php echo ucfirst($found);?></td>
											<td data-title="Destination"><?php echo ucfirst($super);?></td>
											<td data-title="Destination"><?php echo ucfirst($roof);?></td>
										    <td data-title="Destination"><?php echo ucfirst($doors);?></td>
										    <td data-title="Destination"><?php echo ucfirst($floor);?></td>
										    <td data-title="Destination"><?php echo ucfirst($any);?></td>
										    <td data-title="Destination"><?php echo ucfirst($brief);?></td> -->
											<td data-title="Edit"><a href="arch_project_edit.php?id=<?php echo $id;?>">Edit</a></td>
											<?php 
											$where="project_id ='".$id."'";
                                            $rows =  select_all_rows("costford_project_image", $where, $conn, true);
                                            if(isset($rows))
                                            { 
                                            ?>
                                            <td data-title="Edit"><a href="arch_project_editimage.php?id=<?php echo $id;?>">View</a></td>
                                            <?php
                                            }
                                            else
                                            {
                                         	
                                         	?>
                                         	<td data-title="Edit"><a href="arch_project_editadd.php?id=<?php echo $id;?>">Add</a></td>
                                         	<?php
                                            }
                                            ?>

											<?php if($status == 1)
                                            {
                                                ?>
                                                  <td data-title="Delete" style="color: green">Published</td>
                                                <?php
                                            } ?>
                                             <?php if($status == 0)
                                           {
                                                ?>
                                                 <td data-title="Delete" style="color: red">UnPublished</td>
                                                    
                                                <?php
                                            } ?>
											<td data-title="Delete"><a href="arch_project_list.php?delid=<?php echo $id;?>" onclick ="return del()">Delete</a></td>
										</tr>
								<?php
								    }
								}
								    ?>
								   
								    </tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
		<?php include("includes/footer.php");
		?>
	<script src="../assets/scripts/app.js"></script>
	<script src="../assets/scripts/table-editable.js"></script>    
	<script>
		jQuery(document).ready(function() {       
		   App.init();
		   TableEditable.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>