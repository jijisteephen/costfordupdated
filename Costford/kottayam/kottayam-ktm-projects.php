<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once("../".PJI_COR_DIR . "utility.php");
require_once("../".PJI_COR_DIR . "admin-utility.php");
$dyn_folder = "../".PJI_IMG_DIR . PJI_ARA_DIR ;
$dyn_folder1 = "../".PJI_IMG_DIR . PJI_APR_DIR ;
?>
<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Costford - Projects</title>
<!-- for-meta-tags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Health Plus Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-meta-tags-->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- Portfolio-CSS -->	<link rel="stylesheet" href="../css/swipebox.css" type="text/css" media="all">
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script> -->
<script type="text/javascript">
	$(document).ready(function()
	{
		// alert("");
		$("#location").change(function()
		{
			var location = $("#location").val();
			window.location.href = "thiruvananthapuram-tvm-projects.php";

		});
		$("#sqfeet").change(function()
		{
			var sqfeet = $("#sqfeet").val();
			if (sqfeet == 1)
			{
				window.location.href = "thiruvananthapuram-sqfeet1-projects.php";
			}
			if (sqfeet == 2)
			{
				window.location.href = "thiruvananthapuram-sqfeet2-projects.php";
			}
		});
	})
</script>
<style>
#MyForm{
 width: 100%;
    border: 1px solid #e68787;
    padding: 14px;
    background: #fde5e5;
}	
#Myimg{
	margin:0 auto;
  	background: #ccc;
    border: 1px solid #000;
    padding: 9px;
}
</style>
</head>
	<script>
$(document).ready(function(){
	// alert("");
	$('#imgmodal').click(function(){
		// alert("hai");
  		$('#Mymodal').modal('show')
	});
	$('#imgmodal2').click(function(){
		// alert("hai");
  		$('#Mymodal2').modal('show')
	});
	$('#imgmodal3').click(function(){
		// alert("hai");
  		$('#Mymodal3').modal('show')
	});

$('#imgmodal4').click(function(){
		// alert("hai");
  		$('#Mymodal4').modal('show')
});
	$('#imgmodal5').click(function(){
		// alert("hai");
  		$('#Mymodal5').modal('show')
});
$('#primodal').click(function(){
	$('#private').modal('show')
});
$('#primodal1').click(function(){
	$('#private1').modal('show')
});
});
</script>
<body>
<div class="main" id="home">
<!-- banner -->
		<div class="header_agileinfo">
						<div class="w3_agileits_header_text">
								<!-- <ul class="top_agile_w3l_info_icons">
									<li><i class="fa fa-home" aria-hidden="true"></i>12K Street,New York City.</li>
									<li class="second"><i class="fa fa-phone" aria-hidden="true"></i>(+000) 123 456 87</li>
									
									<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:maria@example.com">info@example.com</a></li>
								</ul> -->
								<img src="../img/COSTFORD LOGO.png" width="65%" height="65%">

						</div>
						<div class="agileinfo_social_icons">
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"> </div>
			</div>				

			<div class="header-bottom">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<!-- <div class="logo">
						<h1><a class="navbar-brand" href="index.php"><span>H</span>ealth <i class="fa fa-plus" aria-hidden="true"></i> <p>Quality Care 4U</p></a></h1>
					</div> -->
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--sebastian">
					<ul id="m_nav_list" class="m_nav menu__list">
						<li class="m_nav_item menu__item menu__item" id="m_nav_item_1"> <a href="../index.php" class="menu__link"> Home </a></li>
						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">About Us  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../founder.php">Founders & Leaders</a></li>
									<li><a href="../history.php">History</a></li>
									<li><a href="../approch.php">Approach</a></li>
									<li><a href="../vissionmission.php">Vision,Mission</a></li>
									<li><a href="../governing.php">Governing Body</a></li>
									<li><a href="../typography.php">Sister Organisations</a></li>
								</ul>
						</li>		
						<li class="m_nav_item menu__item" id="moble_nav_item_4"> <a href="../activities.php" class="menu__link">Activities  </a> </li>

						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Architecture  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../projects.php">Projects</a></li>
									<li><a href="../approach.php">Approach</a></li>
									<li><a href="../material.php">Material & Technology</a></li>
									<li><a href="../howbuild.php">How to build with Us</a></li>
								</ul>
						</li>
						
						<li class="m_nav_item menu__item--current" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Costford Centres  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="#">THRISSUR</a></li>
									<li><a href="../thiruvananthapuram/thiruvananthapuram.php" target="_blank">THIRUVANANTHAPURAM</a></li>
									<li><a href="#">KOLLAM</a></li>
									<li><a href="kottayam.php">KOTTAYAM</a></li>
									<li><a href="#">ALAPPUZHA</a></li>
									<li><a href="#">ERNAKULAM</a></li>
									<li><a href="#">THRIPRAYAR</a></li>
									<li><a href="#">THRISSUR 1</a></li>
									<li><a href="#">THRISSUR 2</a></li>
									<li><a href="#">SHORANUR</a></li>
									<li><a href="#">PALAKKAD</a></li>
									<li><a href="#">MALAPPURAM</a></li>
									<li><a href="#">KOZHIKODE</a></li>

								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_6 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Publications <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../publication.php">All Publications</a></li>
									<!-- <li><a href="otherpublication.php">Other Publications</a></li> -->
									<li><a href="../newsevent.php">News & Events</a></li>
								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_6"> <a href="../contact.php" class="menu__link"> Contact </a> </li>
					</ul>
				 </nav>
				</div>
				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1" >
					<nav class="menu menu--sebastian">
					<ul id="m_nav_list" class="m_nav menu__list" style="font-family:'Open Sans', Arial, sans-serif;margin-right:27px; font-size: 86%;margin-top: .8%;color: #520203;">
						<a href ="thiruvananthapuram.php" style="color: #520203;"><b>THIRUVANANTHAPURAM CENTRE</b></a>
					</ul>
				</nav>

				</div>
				<!-- /.navbar-collapse -->
			</nav>
	 </div>
</div>
<!-- banner -->
<!-- banner1 -->

<!-- //banner1 -->
<style type="text/css">
	
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
	font-size: 1em;
    color: #0a0909;
    border-top: none !important;
    padding: 0 !important;
    border: 1px solid #d8bbba;
}
.selectclass {
    margin-left: 84%;
    height: 30px;
    width: 13%;
    text-align: center;
    background-color: #520203;
    color: #fff;
    margin-bottom: 1%;
    float: left;
    border-radius: 5px;
}

.table-bordered {
    border: 1px solid #d8bbba;
}	
.content-wrap section {
    display: none;
    margin: 0 auto;
    padding: 0;
     text-align: left; 
}
</style>

<!-- Portfolio -->
	<div class="portfolio" id="specials" style="padding: 0em 0px;">
			<style type="text/css">
		.btttn
		{
			    margin-left: 7.8%;
    border-radius: 4px;
    color: #fff;
    background-color: #520203;
		}
	</style>
<a href="thiruvananthapuram.php">
	<input type="button" name="" value="<< Back" class="btttn">

</a>
			<div class="container">
				<!-- <h2 class="w3_heade_tittle_agile"></h2> -->
		    <!-- <p class="sub_t_agileits">Quality Care...</p> -->
<nav>
					<select style="margin-left: 58%" class="selectclass" id="location">
						<option value="select">Filter by location</option>
						<option value="tvm">Thiruvananthapuram</option>
					</select>
					<select style="margin-left: 1%" class="selectclass">
						<option>Filter by rates</option>
					</select>
					<select style="margin-left: 1%"  class="selectclass" id="sqfeet">
						<option value="select">Filter by sqft</option>
						<option value="1">500 to 1500 </option>
						<option value="2">1500 to 2500 </option>
						<option value="3">2500 to 3000 </option>
					</select>
				</nav>

					
			<div class="tabs tabs-style-bar">
				<nav>
					<ul>
						<li><a href="#section-bar-1" class="icon icon-box"><span>Location : Thiruvananthapuram</span></a></li>
						<!-- <li><a href="#section-bar-2" class="icon icon-display"><span>Private</span></a></li> -->
						<!-- <li><a href="#section-bar-3" class="icon icon-upload"><span>Institutional</span></a></li>
						<li><a href="#section-bar-4" class="icon icon-upload"><span>Social Housing</span></a></li> -->
					</ul>
				</nav>

				<div class="content-wrap">

				<section id="section-bar-1" class="agileits w3layouts">
						<div class="gallery-grids">
							<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=1";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     $image  = $row['project_frontimage']; 
	                                     
	                                     ?>
							<div class="col-md-4 col-sm-4 gallery-top" >
								
										<img src="<?php echo($dyn_folder.$image);?>" alt="Costford" class="img-responsive" id="imgmodal" style="cursor: pointer;height: 226px;width: 99%">

										<figcaption></figcaption>
									<!-- </figure> -->
								<!-- </a> -->
							</div><?php }}?>
							<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=2";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     $image  = $row['project_frontimage']; 
	                                     
	                                     ?>
							<div class="col-md-4 col-sm-4 gallery-top" >
								
										<img src="<?php echo($dyn_folder.$image);?>" alt="Costford" class="img-responsive" id="imgmodal2" style="cursor: pointer;height: 226px;width: 99%">

										<figcaption></figcaption>
									
							</div><?php }}?>
							<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=3";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     $image  = $row['project_frontimage']; 
	                                     
	                                     ?>
							<div class="col-md-4 col-sm-4 gallery-top"  >
								
										<img src="<?php echo($dyn_folder.$image);?>" alt="Costford" class="img-responsive" id="imgmodal3" style="cursor: pointer;height: 226px;width: 99%">

										<figcaption></figcaption>
									<!-- </figure> -->
								<!-- </a> -->
							</div>
							<?php }}?>
							<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=4";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     $image  = $row['project_frontimage']; 
	                                     
	                                     ?>
							<div class="col-md-4 col-sm-4 gallery-top" style="margin-top:.5%;margin-bottom:.5%">
								
										<img src="<?php echo($dyn_folder.$image);?>" alt="Costford" class="img-responsive" id="imgmodal4" style="cursor: pointer;height: 226px;width: 99%">

										<figcaption></figcaption>
									<!-- </figure> -->
								<!-- </a> -->
							</div><?php }}?>
							<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=5";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     $image  = $row['project_frontimage']; 
	                                     
	                                     ?>
							<div class="col-md-4 col-sm-4 gallery-top" style="margin-top:.5%;margin-bottom:.5%">
								
										<img src="<?php echo($dyn_folder.$image);?>" alt="Costford" class="img-responsive" id="imgmodal5" style="cursor: pointer;height: 226px;width: 99%">

										<figcaption></figcaption>
									<!-- </figure> -->
								<!-- </a> -->
							</div><?php }}?>

							<!-- .modal1 Starts -->
<div class="modal fade" id="Mymodal" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
 					
					<table class="table table-bordered">
						<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=1";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     
	                                     $name =$row['project_name'];
	                                     $type = $row['project_type'];
	                                     $cname = $row['project_cname'];
	                                     $ctype = $row['project_ctype'];
	                                     $canname= $row['project_canname'];
	                                     $locality= $row['project_locality'];
	                                     $district=$row['project_district'];
	                                     $state=$row['project_state'];
	                                     $subcentre=$row['project_subcentre'];
	                                     $service=$row['project_service'];
	                                     $year=$row['project_year'];
	                                     $cost=$row['project_cost'];
	                                     $current=$row['project_currentcost'];
	                                     $area=$row['project_area'];
	                                     $specific=$row['project_specific'];
	                                     $found=$row['project_found'];
	                                     $super=$row['project_super'];
	                                     $roof=$row['project_roof'];
	                                     $doors=$row['project_doors'];
	                                     $floor=$row['project_floor'];
	                                     $any=$row['project_any'];
	                                     $brief=$row['project_brief'];
	                                     
	                                     ?>
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td><?php echo $name;?></td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td><?php echo $type?></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td><?php echo $ctype?> </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td><?php echo $cname?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td><?php echo $canname?> </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td><?php echo $locality?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td><?php echo $district?> </td>
							</tr>
							<tr>
								<td>State</td>
								<td><?php echo $state?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td><?php echo $subcentre?> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td><?php echo $service?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td><?php echo $year?> </td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td><?php echo $cost?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td><?php echo $current?>  </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td><?php echo $area?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td><?php echo $specific?>     </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td><?php echo $found?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td><?php echo $super?> </td>
							</tr>
							<tr>
								<td>Roof</td>
								<td><?php echo $roof?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td><?php echo $doors?> </td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td><?php echo $floor?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr>
								<td>Any other</td>
								<td>    <?php echo $any?>    </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>  <?php echo $brief?>      </td>
							</tr>
							
						</tbody>
						<?php }}?> 
					</table>                   
				</div>
				
			   <div class="clearfix"> </div>
			</div>	
			<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=1";
                                         $rows = select_all_rows("costford_project_image", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
										 $image  = $row['image_image']; 
										 $image1  = $row['image_image1'];
										 $image2  = $row['image_image2'];
										 $image3  = $row['image_image3'];
										 $image4  = $row['image_image4'];
										 $image5  = $row['image_image5'];
	                                     
	                                     ?>
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top" >
								<a href="<?php echo($dyn_folder1.$image1);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image1);?>" alt="Costford" class="img-responsive" style="height: 236px;">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image2);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image2);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image3);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image3);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image4);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image4);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image5);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image5);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div><?php }}?>
							
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
							<!-- Modal 1 ends  -->
							
							<!-- modal 2 start -->
							<div class="modal fade" id="Mymodal2" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
				 <table class="table table-bordered">
						<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=2";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     
	                                     $name =$row['project_name'];
	                                     $type = $row['project_type'];
	                                     $cname = $row['project_cname'];
	                                     $ctype = $row['project_ctype'];
	                                     $canname= $row['project_canname'];
	                                     $locality= $row['project_locality'];
	                                     $district=$row['project_district'];
	                                     $state=$row['project_state'];
	                                     $subcentre=$row['project_subcentre'];
	                                     $service=$row['project_service'];
	                                     $year=$row['project_year'];
	                                     $cost=$row['project_cost'];
	                                     $current=$row['project_currentcost'];
	                                     $area=$row['project_area'];
	                                     $specific=$row['project_specific'];
	                                     $found=$row['project_found'];
	                                     $super=$row['project_super'];
	                                     $roof=$row['project_roof'];
	                                     $doors=$row['project_doors'];
	                                     $floor=$row['project_floor'];
	                                     $any=$row['project_any'];
	                                     $brief=$row['project_brief'];
	                                     
	                                     ?>
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td><?php echo $name;?></td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td><?php echo $type?></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td><?php echo $ctype?> </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td><?php echo $cname?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td><?php echo $canname?> </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td><?php echo $locality?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td><?php echo $district?> </td>
							</tr>
							<tr>
								<td>State</td>
								<td><?php echo $state?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td><?php echo $subcentre?> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td><?php echo $service?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td><?php echo $year?> </td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td><?php echo $cost?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td><?php echo $current?>  </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td><?php echo $area?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td><?php echo $specific?>     </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td><?php echo $found?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td><?php echo $super?> </td>
							</tr>
							<tr>
								<td>Roof</td>
								<td><?php echo $roof?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td><?php echo $doors?> </td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td><?php echo $floor?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr >
								<td>Any other</td>
								<td>    <?php echo $any?>    </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>  <?php echo $brief?>      </td>
							</tr>
							
						</tbody>
						<?php }}?> 
					</table>                        
				</div>
				<?php ?>
			   <div class="clearfix"> </div>
			</div>	
			<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=2";
                                         $rows = select_all_rows("costford_project_image", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
										 $image  = $row['image_image']; 
										 $image1  = $row['image_image1'];
										 $image2  = $row['image_image2'];
										 $image3  = $row['image_image3'];
										 $image4  = $row['image_image4'];
										 $image5  = $row['image_image5'];
	                                     
	                                     ?>
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image1);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image1);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image2);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image2);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image3);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image3);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image4);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image4);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image5);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image5);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div><?php }}?>
								
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
							<!-- modal 2 end -->

							<!-- Modal 3 starts  -->
							<div class="modal fade" id="Mymodal3" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
				 <table class="table table-bordered">
						<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=3";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     
	                                     $name =$row['project_name'];
	                                     $type = $row['project_type'];
	                                     $cname = $row['project_cname'];
	                                     $ctype = $row['project_ctype'];
	                                     $canname= $row['project_canname'];
	                                     $locality= $row['project_locality'];
	                                     $district=$row['project_district'];
	                                     $state=$row['project_state'];
	                                     $subcentre=$row['project_subcentre'];
	                                     $service=$row['project_service'];
	                                     $year=$row['project_year'];
	                                     $cost=$row['project_cost'];
	                                     $current=$row['project_currentcost'];
	                                     $area=$row['project_area'];
	                                     $specific=$row['project_specific'];
	                                     $found=$row['project_found'];
	                                     $super=$row['project_super'];
	                                     $roof=$row['project_roof'];
	                                     $doors=$row['project_doors'];
	                                     $floor=$row['project_floor'];
	                                     $any=$row['project_any'];
	                                     $brief=$row['project_brief'];
	                                     
	                                     ?>
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td><?php echo $name;?></td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td><?php echo $type?></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td><?php echo $ctype?> </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td><?php echo $cname?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td><?php echo $canname?> </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td><?php echo $locality?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td><?php echo $district?> </td>
							</tr>
							<tr>
								<td>State</td>
								<td><?php echo $state?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td><?php echo $subcentre?> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td><?php echo $service?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td><?php echo $year?> </td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td><?php echo $cost?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td><?php echo $current?>  </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td><?php echo $area?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td><?php echo $specific?>     </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td><?php echo $found?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td><?php echo $super?> </td>
							</tr>
							<tr>
								<td>Roof</td>
								<td><?php echo $roof?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td><?php echo $doors?> </td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td><?php echo $floor?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>   Mud plaster  </td>
							</tr>
							<tr >
								<td>Any other</td>
								<td>    <?php echo $any?>    </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>  <?php echo $brief?>      </td>
							</tr>
							
						</tbody>
						<?php }}?> 
					</table>                        
				</div>
				<?php ?>
			   <div class="clearfix"> </div>
			</div>	
			<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=3";
                                         $rows = select_all_rows("costford_project_image", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
										 $image  = $row['image_image']; 
										 $image1  = $row['image_image1'];
										 $image2  = $row['image_image2'];
										 $image3  = $row['image_image3'];
										 $image4  = $row['image_image4'];
										 $image5  = $row['image_image5'];
	                                     
	                                     ?>
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image1);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image1);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image2);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image2);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image3);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image3);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image4);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image4);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image5);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image5);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div><?php }}?>
								
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
                                <!-- Modal 3 ends -->
	
	                              <!-- Modal 4 starts -->
								  <div class="modal fade" id="Mymodal4" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
				 <table class="table table-bordered">
						<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=4";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     
	                                     $name =$row['project_name'];
	                                     $type = $row['project_type'];
	                                     $cname = $row['project_cname'];
	                                     $ctype = $row['project_ctype'];
	                                     $canname= $row['project_canname'];
	                                     $locality= $row['project_locality'];
	                                     $district=$row['project_district'];
	                                     $state=$row['project_state'];
	                                     $subcentre=$row['project_subcentre'];
	                                     $service=$row['project_service'];
	                                     $year=$row['project_year'];
	                                     $cost=$row['project_cost'];
	                                     $current=$row['project_currentcost'];
	                                     $area=$row['project_area'];
	                                     $specific=$row['project_specific'];
	                                     $found=$row['project_found'];
	                                     $super=$row['project_super'];
	                                     $roof=$row['project_roof'];
	                                     $doors=$row['project_doors'];
	                                     $floor=$row['project_floor'];
	                                     $any=$row['project_any'];
	                                     $brief=$row['project_brief'];
	                                     
	                                     ?>
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td><?php echo $name;?></td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td><?php echo $type?></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td><?php echo $ctype?> </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td><?php echo $cname?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td><?php echo $canname?> </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td><?php echo $locality?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td><?php echo $district?> </td>
							</tr>
							<tr>
								<td>State</td>
								<td><?php echo $state?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td><?php echo $subcentre?> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td><?php echo $service?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td><?php echo $year?> </td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td><?php echo $cost?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td><?php echo $current?>  </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td><?php echo $area?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td><?php echo $specific?>     </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td><?php echo $found?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td><?php echo $super?> </td>
							</tr>
							<tr>
								<td>Roof</td>
								<td><?php echo $roof?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td><?php echo $doors?> </td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td><?php echo $floor?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr >
								<td>Any other</td>
								<td>  Mud plaster  <?php echo $any?>    </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>  <?php echo $brief?>      </td>
							</tr>
							
						</tbody>
						<?php }}?> 
					</table>                        
				</div>
				<?php ?>
			   <div class="clearfix"> </div>
			</div>	
			<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=4";
                                         $rows = select_all_rows("costford_project_image", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
										 $image  = $row['image_image']; 
										 $image1  = $row['image_image1'];
										 $image2  = $row['image_image2'];
										 $image3  = $row['image_image3'];
										 $image4  = $row['image_image4'];
										 $image5  = $row['image_image5'];
	                                     
	                                     ?>
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image1);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image1);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image2);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image2);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image3);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image3);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image4);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image4);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image5);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image5);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div><?php }}?>
								
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
								  <!-- Modal 4 ends -->
								  <div class="modal fade" id="Mymodal5" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
				 <table class="table table-bordered">
						<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=5";
                                         $rows = select_all_rows("costford_arch_project", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
	                                     
	                                     $name =$row['project_name'];
	                                     $type = $row['project_type'];
	                                     $cname = $row['project_cname'];
	                                     $ctype = $row['project_ctype'];
	                                     $canname= $row['project_canname'];
	                                     $locality= $row['project_locality'];
	                                     $district=$row['project_district'];
	                                     $state=$row['project_state'];
	                                     $subcentre=$row['project_subcentre'];
	                                     $service=$row['project_service'];
	                                     $year=$row['project_year'];
	                                     $cost=$row['project_cost'];
	                                     $current=$row['project_currentcost'];
	                                     $area=$row['project_area'];
	                                     $specific=$row['project_specific'];
	                                     $found=$row['project_found'];
	                                     $super=$row['project_super'];
	                                     $roof=$row['project_roof'];
	                                     $doors=$row['project_doors'];
	                                     $floor=$row['project_floor'];
	                                     $any=$row['project_any'];
	                                     $brief=$row['project_brief'];
	                                     
	                                     ?>
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td><?php echo $name;?></td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td><?php echo $type?></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td><?php echo $ctype?> </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td><?php echo $cname?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td><?php echo $canname?> </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td><?php echo $locality?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td><?php echo $district?> </td>
							</tr>
							<tr>
								<td>State</td>
								<td><?php echo $state?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td><?php echo $subcentre?> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td><?php echo $service?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td><?php echo $year?> </td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td><?php echo $cost?>  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td><?php echo $current?>  </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td><?php echo $area?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td><?php echo $specific?>     </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td><?php echo $found?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td><?php echo $super?> </td>
							</tr>
							<tr>
								<td>Roof</td>
								<td><?php echo $roof?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td><?php echo $doors?> </td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td><?php echo $floor?> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr >
								<td>Any other</td>
								<td>    <?php echo $any?>    </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>  <?php echo $brief?>      </td>
							</tr>
							
						</tbody>
						<?php }}?> 
					</table>                        
				</div>
				<?php ?>
			   <div class="clearfix"> </div>
			</div>	
			<?php 
 	                                     $where="b_active='1' and status = 1 and project_id=5";
                                         $rows = select_all_rows("costford_project_image", $where, $conn, true);
                                         $slno=1; 
                                        if(isset($rows))
                 	                    {
                	                    foreach($rows as $row)
                	                    {
										 $image  = $row['image_image']; 
										 $image1  = $row['image_image1'];
										 $image2  = $row['image_image2'];
										 $image3  = $row['image_image3'];
										 $image4  = $row['image_image4'];
										 $image5  = $row['image_image5'];
	                                     
	                                     ?>
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image1);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image1);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image2);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image2);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image3);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image3);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image4);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image4);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="<?php echo($dyn_folder1.$image5);?>" class="swipebox">
									<figure class="effect-apollo">
										<img src="<?php echo($dyn_folder1.$image5);?>" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div><?php }}?>
								
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
                                                <!--  Modal 5 ends -->
							<div class="clearfix"></div>
						</div>
					</section>


					<!-- //Tab-1 -->

					<!-- Tab-2 -->
					<section id="section-bar-2" class="agileits w3layouts">
						<div class="gallery-grids">
							<div class="col-md-4 col-sm-4 gallery-top" style="margin-bottom: 10px;">
								<!-- <a href="../images/approach/DSC_0920.jpg" class="swipebox">
									<figure class="effect-apollo"> -->
										<img src="../images/approach/DSC_0920.jpg" alt="Costford" class="img-responsive" id="primodal" style="cursor: pointer;height:226px;width:99%">
										<figcaption></figcaption>
									<!-- </figure>
								</a> -->
							</div>

							<div class="col-md-4 col-sm-4 gallery-top" style="margin-bottom: 10px;">
								<!-- <a href="../images/approach/5.jpg" class="swipebox">
									<figure class="effect-apollo"> -->
										<img src="../images/approach/5.jpg" alt="Costford" class="img-responsive" id="primodal1" style="cursor: pointer;height:226px;width:99%">
										<figcaption></figcaption>
									<!-- </figure>
								</a> -->
							</div>
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g2.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g2.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g3.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g3.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g5.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g5.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g6.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g6.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
 <!-- .modal1 Starts -->
<div class="modal fade" id="private" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
 					
					<table class="table table-bordered">
						
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td>Institute of Management in Government hostel</td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td>Institutional</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td>Government </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td>Institute of Management in Government </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td>Yes </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td>Thiruvananthapuram </td>
							</tr>
							<tr>
								<td>State</td>
								<td>Kerala  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td>Consultancy and construction / Construction only / DPR only / PMC</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td></td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td> </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td>    </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td></td>
							</tr>
							<tr>
								<td>Roof</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td></td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr>
								<td>Any other</td>
								<td>        </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>     </td>
							</tr>
							
						</tbody>
						
					</table>                   
				</div>
				
			   <div class="clearfix"> </div>
			</div>	
			
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0838.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0838.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top" >
								<a href="../images/img/DSC_0920.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0920.jpg" alt="Costford" class="img-responsive" style="height: 236px;">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0919.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0919.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0921.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0921.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0870.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0870.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/DSC_0846.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/DSC_0846.jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div>
							
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>

<!-- modal 2 starts -->
<div class="modal fade" id="private1" style="width: 62%; margin-left: 20%">
	<div class="modal-dialog" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:     #fff;">×</button> 
				<h4 class="modal-title">
                	Project Details
                </h4>                                                             
			</div> 
			
			<div class="modal-body">
			
				<div class="grid_3 grid_5 wthree">
 				<div class="col-md-12 agileits-w3layouts">
 					
					<table class="table table-bordered">
						
						<tbody>
							<tr style="background-color: #d0c9c9;">
								<td>Project Name</td>
								<td>Karimadom Slum development</td>
							</tr>
							<tr>
								<td>Project Type</td>
								<td>Special projects: Slum Development</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Customer type</td>
								<td>Government </td>
							</tr>
							<tr>
								<td>Customer name</td>
								<td> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Can customer name be published</td>
								<td>Yes </td>
							</tr>
							<tr>
								<td>Locality</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>District</td>
								<td>Thiruvananthapuram </td>
							</tr>
							<tr>
								<td>State</td>
								<td>Kerala  </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Sub Centre name</td>
								<td> </td>
							</tr>
							<tr>
								<td>Type of Service provided</td>
								<td>Consultancy and construction / Construction only / DPR only / PMC</td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Year of Construction</td>
								<td></td>
							</tr>
							<tr>
								<td>Cost of construction</td>
								<td> </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Cost at current rates</td>
								<td> </td>
							</tr>
							<tr>
								<td>Total Built-up area</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Brief specifications</td>
								<td>    </td>
							</tr>
							<tr>
								<td>Foundation</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Superstructure</td>
								<td></td>
							</tr>
							<tr>
								<td>Roof</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Doors and Windows</td>
								<td></td>
							</tr>
							<tr>
								<td>Floor finish</td>
								<td></td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>Wall finish</td>
								<td>     </td>
							</tr>
							<tr>
								<td>Any other</td>
								<td>        </td>
							</tr>
							<tr style="background-color: #d0c9c9;">
								<td>A brief write-up (not compulsory) not more than 200 words</td>
								<td>     </td>
							</tr>
							
						</tbody>
						
					</table>                   
				</div>
				
			   <div class="clearfix"> </div>
			</div>	
			
				<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (1).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (1).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top" >
								<a href="../images/img/1 (2).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (2).jpg" alt="Costford" class="img-responsive" style="height: 236px;">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (3).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (3).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (4).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (4).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (5).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (5).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/img/1 (6).jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/img/1 (6).jpg" alt="Costford" class="img-responsive" style="height: 236px">
										<figcaption></figcaption>
									</figure>
								</a>
										</div>
							
			</div>   
			<div class="modal-footer" style="background-color: #fff;padding: 0px">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #520203;color: #fff;">Close</button>                        
			</div>
		</div>                                                                       
	</div>                                      
</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<!-- //Tab-2 -->

					<!-- Tab-3 -->
					<section id="section-bar-3" class="agileits w3layouts">
						<div class="gallery-grids">
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g7.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g7.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g3.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g3.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g9.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g9.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
	
					

			
							<div class="clearfix"></div>
						</div>
					</section>
					<!-- //Tab-3 -->
					<!-- Tab-4 -->
					<section id="section-bar-4" class="agileits w3layouts">
						<div class="gallery-grids">
							<!-- <div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g7.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g7.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g3.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g3.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div>
							<div class="col-md-4 col-sm-4 gallery-top">
								<a href="../images/g9.jpg" class="swipebox">
									<figure class="effect-apollo">
										<img src="../images/g9.jpg" alt="Costford" class="img-responsive">
										<figcaption></figcaption>
									</figure>
								</a>
							</div> -->
	
					

			
							<div class="clearfix"></div>
						</div>
					</section>
					<!-- //Tab-4 -->
					
				</div><!-- //Content -->
			</div><!-- //Tabs -->

		</div>
	</div>
	<!-- //Portfolio -->




<!-- footer -->
<div class="footer">
		<div class="container">
			
			<div class="agile_footer_copy">
				<div class="w3agile_footer_grids">
					<div class="col-md-4 w3agile_footer_grid">
						<h3>About Us</h3>
						<p style="text-align: justify;">Centre of Science and Technology For Rural Development (COSTFORD) is a Thrissur city-based organisation that gives technological assistance to people in alternative building technology.</p>
					</div>
					<div class="col-md-4 w3agile_footer_grid">
						<h3>Contact Info</h3>
						<ul>
							<li><i class="fa fa-map-marker" aria-hidden="true"></i>The Hamlet, Benedict Nagar,
Nalanchira P.O, <span>Thiruvananthapuram 695015</span></li>
							<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@example.com">costfordtvm@gmail.com</a></li>
							<li><i class="fa fa-phone" aria-hidden="true"></i>0471 - 2530031, 09446540220</li>
						</ul>
					</div>
					<div class="col-md-4 w3agile_footer_grid w3agile_footer_grid1">
						<h3>Navigation</h3>
						<ul>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../gallery.php">Architecture</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../appointment.php">Costford Centres</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../history.php">About</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../contact.php">Contact Us</a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<!--<div class="w3_agileits_copy_right_social">
				<div class="col-md-6 agileits_w3layouts_copy_right">
					<p>&copy; 2020. All rights reserved | Design by <a href="http://grameena.org/">Grameena Patana Kendram</a></p>
				</div>
				<div class="col-md-6 w3_agile_copy_right">
					<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
				</div>
				<div class="clearfix"> </div>
			</div>-->
		</div>
	</div>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
 <!-- js -->
<script src="../js/jquery-2.2.3.min.js"></script>
<!-- Gallery-Tab-JavaScript -->
			<script src="../js/cbpFWTabs.js"></script>
			<script>
				(function() {
					[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
						new CBPFWTabs( el );
					});
				})();
			</script>
		<!-- //Gallery-Tab-JavaScript -->


<!-- Swipe-Box-JavaScript -->
			<script src="../js/jquery.swipebox.min.js"></script> 
				<script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
			</script>
		<!-- //Swipe-Box-JavaScript -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Health Plus
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="../images/g9.jpg" alt=" " class="img-responsive" />
						<p>Ut enim ad minima veniam, quis nostrum 
							exercitationem ullam corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi consequatur? Quis autem 
							vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur, vel illum qui 
							dolorem eum fugiat quo voluptas nulla pariatur.
							<i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
								esse quam nihil molestiae consequatur.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="../js/move-top.js"></script>
<script type="text/javascript" src="../js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
			<script src="../js/jarallax.js"></script>
	<script src="../js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>
	
	<script src="../js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>