
<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once("../".PJI_COR_DIR . "utility.php");
require_once("../".PJI_COR_DIR . "admin-utility.php");
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Costford - Home</title>
<!-- for-meta-tags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Health Plus Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free web designs for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-meta-tags-->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="../css/flexslider.css" type="text/css" media="screen" Department="" />
<link href="../css/services.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css">
<link href="../css/JiSlider.css" rel="stylesheet"> 
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome icons -->
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../icofont.min.css">
<link rel="stylesheet" type="text/css" href="../icofont.css">
</head>
	
<body>
<div class="main" id="home">
<!-- banner -->
		<div class="header_agileinfo">
						<div class="w3_agileits_header_text">
								<!-- <ul class="top_agile_w3l_info_icons">
									<li><i class="fa fa-home" aria-hidden="true"></i>12K Street,New York City.</li>
									<li class="second"><i class="fa fa-phone" aria-hidden="true"></i>(+000) 123 456 87</li>
									
									<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:maria@example.com">info@example.com</a></li>
								</ul> -->
								<img src="../img/COSTFORD LOGO.png" width="65%" height="65%">

						</div>
						<div class="agileinfo_social_icons">
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"> </div>
			</div>				
			<div class="header-bottom">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<!-- <div class="logo">
						<h1><a class="navbar-brand" href="index.php"><span>H</span>ealth <i class="fa fa-plus" aria-hidden="true"></i> <p>Quality Care 4U</p></a></h1>
					</div> -->
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--sebastian">
					<ul id="m_nav_list" class="m_nav menu__list">
						<li class="m_nav_item menu__item menu__item" id="m_nav_item_1"> <a href="../index.php" class="menu__link"> Home </a></li>
						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">About Us  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../founder.php">Founders & Leaders</a></li>
									<li><a href="../history.php">History</a></li>
									<li><a href="../approch.php">Approach</a></li>
									<li><a href="../vissionmission.php">Vision,Mission</a></li>
									<li><a href="../governing.php">Governing Body</a></li>
									<li><a href="../typography.php">Sister Organisations</a></li>
								</ul>
						</li>		
						<li class="m_nav_item menu__item" id="moble_nav_item_4"> <a href="../activities.php" class="menu__link">Activities  </a> </li>

						<li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Architecture  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../projects.php">Projects</a></li>
									<li><a href="../approach.php">Approach</a></li>
									<li><a href="../material.php">Material & Technology</a></li>
									<li><a href="../howbuild.php">How to build with Us</a></li>
								</ul>
						</li>
						
						<li class="m_nav_item menu__item--current" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Costford Centres  <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="#">THRISSUR</a></li>
									<li><a href="../thiruvananthapuram/thiruvananthapuram.php" target="_blank">THIRUVANANTHAPURAM</a></li>
									<li><a href="#">KOLLAM</a></li>
									<li><a href="kottayam.php">KOTTAYAM</a></li>
									<li><a href="#">ALAPPUZHA</a></li>
									<li><a href="#">ERNAKULAM</a></li>
									<li><a href="#">THRIPRAYAR</a></li>
									<li><a href="#">THRISSUR 1</a></li>
									<li><a href="#">THRISSUR 2</a></li>
									<li><a href="#">SHORANUR</a></li>
									<li><a href="#">PALAKKAD</a></li>
									<li><a href="#">MALAPPURAM</a></li>
									<li><a href="#">KOZHIKODE</a></li>

								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_6 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Publications <b class="caret"></b></a> 
						   <ul class="dropdown-menu agile_short_dropdown">
									<li><a href="../publication.php">All Publications</a></li>
									<!-- <li><a href="otherpublication.php">Other Publications</a></li> -->
									<li><a href="../newsevent.php">News & Events</a></li>
								</ul>
						</li>
						<li class="m_nav_item menu__item" id="moble_nav_item_6"> <a href="../contact.php" class="menu__link"> Contact </a> </li>
					</ul>
				 </nav>
				</div>
				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1" >
					<nav class="menu menu--sebastian">
					<ul id="m_nav_list" class="m_nav menu__list" style="font-family:'Open Sans', Arial, sans-serif;margin-right:27px; font-size: 86%;margin-top: .8%;color: #520203;">
						<a href ="kottayam.php" style="color: #520203;"><b>KOTTAYAM CENTRE</b></a>
					</ul>
				</nav>

				</div>
				<!-- /.navbar-collapse -->
			</nav>
	 </div>
</div>

<!-- banner -->
	<div class="banner-silder">
		<div id="JiSlider" class="jislider">
			<ul>
<?php 
 	// $where="b_active='1' and status = 1";
  //   $rows = select_all_rows("costford_home_slider", $where, $conn, true);
  //   $slno=1; 
  //   if(isset($rows))
  // 	{
  //   	foreach($rows as $row)
  //   	{
	 //      $id   = $row['slider_id'];
	 //      $title  = $row['slider_title'];
	 //      $image  = $row['slider_image']; 
	      ?>
		 
	      <li>
					<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/C.S.I Eco Spirituality Centre.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;">

							<div class="container">
									<div class="agileits-banner-info">
									<!-- <span>Health Plus</span> -->
									<!-- <h3></h3> -->
									 <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>	
							</div>
						</div>
				</li>
	      <?php
	//   }
	// }
	      ?>

				
				<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Dr.Sabu Thomas.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
						<div class="container">
								<div class="agileits-banner-info">
								 <!-- <span>Real</span>
									<h3>Free Consultation </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>	
							</div>
						</div>
				</li>
				<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/2.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Mr.Padmanabha Panicker.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/1.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Mr.V.I Thomas.jpeg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Mr.Vinod.R.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/OOR.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Pallikoodam (2).jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Pallikoodam (3).jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
				    <li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Smt.Usha Kumari.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/Sunday School Building.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top" style="background:  url('../images/ktm/banner/TIES.jpg') no-repeat 0px 0px;background-size: 100% 100%;background-position: 100% 100%;" >
							<div class="container">
								<div class="agileits-banner-info">
								    <!--  <span>Amazing</span>
									<h3>Quality Care </h3>
									 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.</p> -->
									
								</div>
								
							</div>
						</div>
					</li>
			</ul>
		</div>
      </div>

<!-- //banner -->

<!-- about -->
<style type="text/css">
	.bttn
	{
		height: 87px;
    width: 79%;
    border-radius: 15px;
    background-color: #520203;
    color: #fff;
    font-size: 22px;
    margin-left: 4%;
	}
</style>
	<div class="about" id="about">
		<div class="container">
			<!-- <h2 class="w3_heade_tittle_agile">Welcome to our Health Plus</h2>
			<p class="sub_t_agileits">Add Short Description</p>
			
			<p class="ab">Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed blandit massa vel mauris sollicitudin dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at placerat ante. Praesent nulla nunc, pretium dapibus efficitur in, auctor eget elit. Lorem ipsum dolor sit amet</p> -->

			 <div class="about-w3lsrow"> 
				
				<div class="col-md-3 w3about-img"> 
					<div class="w3about-text"> 
						<a href="kottayam-projects.php">
							<input type="button" name="" value="Construction Projects" class="bttn">
						</a>
					</div>
				    <!-- <img src="images/about.jpg" alt=" " class="img-responsive"> -->
				</div> 
				<div class="col-md-3 w3about-img"> 
					<div class="w3about-text"> 
						<input type="button" name="" value="Other Activities" class="bttn">
					</div>
				    <!-- <img src="images/about.jpg" alt=" " class="img-responsive"> -->
				</div> 
				<div class="col-md-3 w3about-img"> 
					<div class="w3about-text"> 
						<a href="kottayam-team.php">
						<input type="button" name="" value="Our Team" class="bttn">
					</a>
					</div>
				    <!-- <img src="images/about.jpg" alt=" " class="img-responsive"> -->
				</div> 
				<div class="col-md-12 w3about-img" style="margin-top: 3%"> 
					<div class="w3about-text"> 
						<h5 class="w3l-subtitle">
							<h3 style="font-size: 2em; font-family: 'open sans' , arial, sans-serif;"><span class="bt_bb_headline_content"><span>ABOUT 
                             KOTTAYAM CENTRE </span></span></h3>
						</h5><br/>
						<p style="text-align: justify; font-family: 'open sans' , arial, sans-serif;">The Kottayam  project centre of COSTFORD was started on 03 March 2004. Er. Biju P John, who has been involved with COSTFORD from 2000, took the lead in setting up this centre. In a short span of time, COSTFORD Kottayam has been able to execute over 300 hundred residential projects, beside a number of Institutional and government projects.
The initial projects of the centre included Laurie Baker’s work – Pallikoodam for which he was making some additions. The tropical Institute of Ecological Sciences at Pampady best exemplifies our approach and principles. We have also built schools, churches, resorts and landscaped public places.
We promote recycling of old building materials especially old doors and windows in a big way. Oor, an old age home near Mallapally, is a model example of construction with reused materials salvaged from demolished old buildings. Over 95% of the materials used in the construction of the structure along with landscape, pathways, and boundary walls use materials salvaged from over 20 buildings.
The centre is currently located at Thaipparambu, near Karukachal, and undertake work in both Kottayam and Pathanamthitta districts .
 </p>
						
				<h1>
					<!-- <a href="history.php"><span class="label label-danger">Read More</span></a></h1> -->
						
					</div>
				    <!-- <img src="images/about.jpg" alt=" " class="img-responsive"> -->
				</div> 
				<div class="col-md-12 w3about-img"> 
					<div class="w3about-text"> 
						
						<marquee><p style="text-align: justify; color: #520203;   font-weight: bold;
 " ;>The Centre of Science and Technology for Rural Development (COSTFORD), is a nonprofit organisation founded in 1985 by Mr. C. Achutha Menon, Kerala’s former Chief Minister; Dr. K.N. Raj, Economist and former Chairman of the Centre for Development Studies (CDS); Laurie Baker, Master Architect; and Mr. T.R. Chandra Dutt, Social Activist.</p></marquee>
						
				<h1>
					
					</div>
				    <!-- <img src="images/about.jpg" alt=" " class="img-responsive"> -->
				</div> 
				<style type="text/css">
					.ico-title {
				font-size: 2em;
			}
			.iconlist {
				margin: 0;
				padding: 0;
				list-style: none;
				text-align: center;
				width: 100%;
				display: flex;
				flex-wrap: wrap;
				flex-direction: row;
			}
			.iconlist li {
				position: relative;
				margin: 5px;
				width: 150px;
				cursor: pointer;
			}
			.iconlist li .icon-holder {
				position: relative;
				text-align: center;
				border-radius: 3px;
				overflow: hidden;
				padding-bottom: 5px;
				background: #ffffff;
				border: 1px solid #E4E5EA;
				transition: all 0.2s linear 0s;
			}
			.iconlist li .icon-holder:hover {
				background: #8c1113;
				color: #ffffff;
			}
			.iconlist li .icon-holder:hover .icon i {
				color: #ffffff;
			}
			.iconlist li .icon-holder .icon {
				padding: 10px;
				text-align: center;
			}
			.iconlist li .icon-holder .icon i {
				font-size: 7em;
				color: #8c1113;
			}
			.iconlist li .icon-holder span {
				font-size: 14px;
				display: block;
				margin-top: 5px;
				border-radius: 3px;
			}
				</style>
		
				<div class="clearfix"> </div>
			</div>
		</div>
</div>
<!-- /about-bottom -->


<!-- footer -->
	<div class="footer">
		<div class="container">
			
			<div class="agile_footer_copy">
				<div class="w3agile_footer_grids">
					<div class="col-md-4 w3agile_footer_grid">
						<h3>About Us</h3>
						<p style="text-align: justify;">Centre of Science and Technology For Rural Development (COSTFORD) is a Thrissur city-based organisation that gives technological assistance to people in alternative building technology.</p>
					</div>
					<div class="col-md-4 w3agile_footer_grid">
						<h3>Contact Info</h3>
						<ul>
							<li><i class="fa fa-map-marker" aria-hidden="true"></i>The Hamlet, Benedict Nagar,
Nalanchira P.O, <span>Thiruvananthapuram 695015</span></li>
							<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@example.com">costfordtvm@gmail.com</a></li>
							<li><i class="fa fa-phone" aria-hidden="true"></i>0471 - 2530031, 09446540220</li>
						</ul>
					</div>
					<div class="col-md-4 w3agile_footer_grid w3agile_footer_grid1">
						<h3>Navigation</h3>
						<ul>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../projects.php">Architecture</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../centres.php">Costford Centres</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../history.php">About</a></li>
							<li><span class="fa fa-long-arrow-right" aria-hidden="true"></span><a href="../contact.php">Contact Us</a></li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<!-- <div class="w3_agileits_copy_right_social">
				<div class="col-md-6 agileits_w3layouts_copy_right">
					<p>&copy; 2020. All rights reserved | Design by <a href="http://grameena.org/">Grameena Patana Kendram</a></p>
				</div>
				<div class="col-md-6 w3_agile_copy_right">
					<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
							</ul>
				</div>
				<div class="clearfix"> </div> 
			</div> -->
		</div>
	</div>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
 <!-- js -->
<script src="../js/jquery-2.2.3.min.js"></script>
<script src="../js/JiSlider.js"></script>
		<script>
			$(window).load(function () {
				$('#JiSlider').JiSlider({color: '#fff', start: 3, reverse: true}).addClass('ff')
			})
		</script><script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script src="../js/ziehharmonika.js"></script>
<script>
$(document).ready(function() {
		$('.ziehharmonika').ziehharmonika({
			collapsible: true,
			prefix: ''
		});
	});
</script>
<!-- stats -->
	<script src="../js/jquery.waypoints.min.js"></script>
	<script src="../js/jquery.countup.js"></script>
		<script>
			$('.counter').countUp();
		</script>
<!-- //stats -->
<script type="text/javascript">
$(function(){
  $("#bars li .bar").each(function(key, bar){
    var percentage = $(this).data('percentage');

    $(this).animate({
      'height':percentage+'%'
    }, 1000);
  })
})
</script>

<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				Costford
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="../images/g9.jpg" alt=" " class="img-responsive" />
						<p>Ut enim ad minima veniam, quis nostrum 
							exercitationem ullam corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi consequatur? Quis autem 
							vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur, vel illum qui 
							dolorem eum fugiat quo voluptas nulla pariatur.
							<i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
								esse quam nihil molestiae consequatur.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->

<!-- flexSlider -->
	<script defer src="../js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
  </script>
<!-- //flexSlider -->


<!-- start-smoth-scrolling -->
<script type="text/javascript" src="../js/move-top.js"></script>
<script type="text/javascript" src="../js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
			<script src="../js/jarallax.js"></script>
	<script src="../js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>
	
	<script src="../js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>